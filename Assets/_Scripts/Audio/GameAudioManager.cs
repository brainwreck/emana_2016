﻿using UnityEngine;

public class GameAudioManager : MonoBehaviour
{
	public AudioSource mainBgmSource;
	public AudioSource[] charBgmSources;

	public AudioClip[] levelBgms;
	public AudioClip[] charBgms;

	private void Start()
	{
		GameEventsManager.OnBuildEnd += SetClips;
	}

	private void OnDisable()
	{
		GameEventsManager.OnBuildEnd -= SetClips;
	}

	private void SetClips()
	{
		string currentLevelName = Application.loadedLevelName;
		int charBgmModifier;

		if (currentLevelName.Contains("Solacio"))
		{
			mainBgmSource.clip = levelBgms[0];
			charBgmModifier = 0;
		}
		else
		{
			mainBgmSource.clip = levelBgms[1];
			charBgmModifier = 3;
		}

		for (int i = 0; i < charBgmSources.Length; i++)
		{
			charBgmSources[i].clip = charBgms[i + charBgmModifier];
		}

		float bgmLength = mainBgmSource.clip.length;
		InvokeRepeating("LoopBgm", 0, bgmLength);
	}

	private void LoopBgm()
	{
		LevelController l = FindObjectOfType<LevelController>();

		mainBgmSource.Play();
		for (int i = 0; i < charBgmSources.Length; i++)
		{
			charBgmSources[i].Play();
		}

		//if (l.level.redFinished || (int)l.currentPlayer == 0) { charBgmSources[0].Play(); }
		//if (l.level.jadeFinished || (int)l.currentPlayer == 1) { charBgmSources[1].Play(); }
		//if (l.level.blueFinished || (int)l.currentPlayer == 2) { charBgmSources[2].Play(); }
	}
}
