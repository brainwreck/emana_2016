﻿using UnityEngine;

public static class AudioOptions
{
	[Range(0, 1)]
	public static float sfxVolume, bgmVolume;
}
