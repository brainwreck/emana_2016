﻿using UnityEngine;
using System.Collections.Generic;

public class SoundBank : MonoBehaviour
{
	[SerializeField]
	private SoundEffect[] effects;
	public Dictionary<EffectType, AudioClip> Effects = new Dictionary<EffectType, AudioClip>();

	private void Awake()
	{
		for (int i = 0; i < effects.Length; i++)
		{
			Effects.Add(effects[i].type, effects[i].clip);
		}
	}
}

[System.Serializable]
public struct SoundEffect
{
	public EffectType type;
	public AudioClip clip;
}

public enum EffectType
{
	Spawn,
	Walk,
	SkillJaspar,
	SkillNadja,
	SkillZafiro,
	Jump,
	Damage,
	Interact,
	Death
};