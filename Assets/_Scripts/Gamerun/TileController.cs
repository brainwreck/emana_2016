﻿using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public class TileController : MonoBehaviour//, IColorProgress
{
	public bool isRed;
	public bool isJade;
	public bool isBlue;

	public	Transform[]	trails;
	public	GameObject[] affectedObjects;

	private void OnEnable ()
	{
		GameEventsManager.OnBuildEnd += GenerateTrails;
	}
	private void OnDisable ()
	{
		GameEventsManager.OnBuildEnd -= GenerateTrails;
	}
	private void Start ()
	{
		List<Transform> trailsList = new List<Transform>(GetComponentsInChildren<Transform>());
		trails = trailsList.FindAll (t => t.name == "Trail").ToArray ();
		PaintTrails();
	}

	public void Paint (bool paintRed, bool paintJade, bool paintBlue)
	{
		if(paintRed) isRed = true;
		if(paintJade) isJade = true;
		if(paintBlue) isBlue = true;

		PaintAllObjects ();
	}

	public void Paint (PlayerColor color)
	{
		switch (color)
		{
			case PlayerColor.Red:
				isRed = true;
				break;
			case PlayerColor.Jade:
				isJade = true;
				break;
			case PlayerColor.Blue:
				isBlue = true;
				break;
		}

		PaintAllObjects ();
		PaintTrails ();
	}

	public void PaintTrails ()
	{
		TrailController[] _trails = GetComponentsInChildren<TrailController> ();
		foreach (var trail in _trails)
			trail.Paint (isRed, isJade, isBlue);
	}

	public void PaintAllObjects ()
	{
		foreach (GameObject item in affectedObjects)
			foreach(IColorProgress colorable in item.GetComponents<IColorProgress>())
				colorable.Paint (isRed, isJade, isBlue);
	}

	private void GenerateTrails ()
	{
		foreach (var trail in trails)
		{
			if (trail.childCount > 0) continue;
			GameObject t = Instantiate (PrefabsHolder.instance.GetRandomTrail(), trail.position, trail.rotation) as GameObject;
			t.transform.SetParent (trail);
		}

		PaintTrails ();
	}
}
