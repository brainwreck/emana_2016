﻿using UnityEngine;
using System.Collections;

public class LevelController : MonoBehaviour
{
	public Level level;
	public PlayerColor currentPlayer;
	public Transform spawnPoint;
	public Vector3 spawnOffset;
	public TileController[] tiles;
	public Transform	spawnablesHolder, limitsHolder;
	public bool success;
    public GameObject successFeedbackObject, failureFeedbackObject;

	private GameObject _player, _cam;

	private void OnEnable ()
	{
		GameEventsManager.OnBuildEnd += BuildEnd;
		GameEventsManager.OnGameEnd += GameEnd;
	}
	private void OnDisable ()
	{
		GameEventsManager.OnBuildEnd -= BuildEnd;
		GameEventsManager.OnGameEnd -= GameEnd;
	}

	private void Awake ()
	{
		if(limitsHolder == null) limitsHolder = transform.FindChild ("CameraLimits");
		if(spawnablesHolder == null) spawnablesHolder = transform.FindChild ("Spawnables");
	}

	public void SetPlayer(int id)
	{
		currentPlayer = (PlayerColor)id;
		CheatsController.instance.endPanel.SetActive(false);
	}

	public void BuildEnd () { StartCoroutine (BuildEndRoutine ()); }
	private IEnumerator BuildEndRoutine ()
	{
		//currentPlayer = level.GetRandomAvailablePlayer ();

		/*tiles = GetComponentsInChildren<TileController>();
		PlayerColor ownerColor = level.levelOwner;

		if (tiles != null)
		{
			for (int i = 0; i < tiles.Length; i++)
			{
				tiles[i].Paint(ownerColor);
			}
		while (CheatsController.instance.endPanel.activeSelf || _player != null)
		{
			yield return null;
		}
		}*/

		_player = Instantiate(PrefabsHolder.instance.GetPlayer((int)currentPlayer), spawnPoint.position + Vector3.up, Quaternion.identity) as GameObject;
		switch ((int)currentPlayer)
		{
			case 0:
				_player.GetComponent<PlayerStats>().energy = level.JasparLife;
				break;
			case 1:
				_player.GetComponent<PlayerStats>().energy = level.NadjaLife;
				break;
			case 2:
				_player.GetComponent<PlayerStats>().energy = level.ZafiroLife;
				break;
		}

		if (_cam == null) _cam = Instantiate(PrefabsHolder.instance.gameCamera, spawnPoint.position - Vector3.forward * 15 + Vector3.up * 5, PrefabsHolder.instance.gameCamera.transform.rotation) as GameObject;
		_cam.GetComponent<GameCamera>().Init(limitsHolder);

		/*StartCoroutine (LevelBuilder.instance.howTos [currentPlayer].Fade (true, .5f));
		yield return new WaitForSeconds (3);
		LevelBuilder.instance.continueText.SetActive (true);
		while(true)
		{
			if(Input.anyKey)
			{
				StartCoroutine(LevelBuilder.instance.loadingBg.Fade (false, .5f));
				break;
			}
			yield return null;
		}
		LevelBuilder.instance.continueText.SetActive (false);
		LevelBuilder.instance.howTos [currentPlayer].gameObject.SetActive (false);

		if (level.spawnables != null)
		{
			for (int i = 0; i < spawnablesHolder.childCount; i++)
			{
				Destroy (spawnablesHolder.GetChild (i).gameObject);
			}

			Vector3 pos;
			Quaternion rot;

			var toggleableDTOs = level.GetToggleableDTOs ();
			for (int toggle = 0; toggle < toggleableDTOs.Count; toggle++)
			{
				var refObj = toggleableDTOs[toggle];
				pos = new Vector3 (refObj.XPos, refObj.YPos, refObj.ZPos);
				rot = Quaternion.Euler (new Vector3 (refObj.XRot, refObj.YRot, refObj.ZRot));
				GameObject obj = Instantiate (Resources.Load ("Spawnables/" + refObj.PrefabName), pos, rot) as GameObject;
				obj.GetComponent<Toggleable>().state = refObj.state;
			}

			var spawnableDTOs = level.GetSpawnableDTOs ();
			for (int j = 0; j < spawnableDTOs.Count; j++)
			{
				var refObj = spawnableDTOs [j];
				pos = new Vector3 (refObj.XPos, refObj.YPos, refObj.ZPos);
				rot = Quaternion.Euler (new Vector3 (refObj.XRot, refObj.YRot, refObj.ZRot));
				Instantiate (Resources.Load ("Spawnables/" + refObj.PrefabName), pos, rot);
			}
		}

		*/

		//GameEventsManager.GameStart ();
		yield return null;
	}

	private void GameEnd () { StartCoroutine (GameEndRoutine()); }
	private IEnumerator GameEndRoutine ()
	{
		Destroy (_player);

 		string charName = string.Empty;
		if (currentPlayer == PlayerColor.Red) charName = "Jaspar: ";
		if (currentPlayer == PlayerColor.Jade) charName = "Nadja: ";
		if (currentPlayer == PlayerColor.Blue) charName = "Zafiro: ";

		int progress = 0;
        if (success)
		{
			success = false;
			string levelName = Application.loadedLevelName;

			switch ((int)currentPlayer)
			{
				case 0:
					level.redFinished = true;
				    break;
				case 1:
					level.jadeFinished = true;
					break;
				case 2:
					level.blueFinished = true;
					break;
			}

			progress += level.redFinished ? 1 : 0;
			progress += level.jadeFinished ? 1 : 0;
			progress += level.blueFinished ? 1 : 0;

			if (level.finished) 
			{
				if (MainManager.instance.myProgress == level.levelId) MainManager.instance.myProgress++;

				yield return new WaitForSeconds(2f);
				while (TextBox.instance.Activated) { yield return null; }

				SceneManager.Instance.GoToLevelSelect();
			}
			else
			{
				currentPlayer = level.playerOrder[progress];
			}
		}
		else
		{
			ClearSpawnables(currentPlayer);

			TextBox.instance.Activate(string.Format("{0}{1}", charName, PrefabsHolder.instance.deathText), _player.transform.position);
		}

		while (TextBox.instance.Activated) { yield return null; }

		if (progress != 3) LevelBuilder.instance.Build();

		yield break;
	}

	private void ClearSpawnables(PlayerColor color)
	{
		Transform t = null;

		switch ((int)currentPlayer)
		{
			case 0:
				t = PrefabsHolder.instance.transform.FindChild("JasparSpawnableHolder");
				break;
			case 1:
				t = PrefabsHolder.instance.transform.FindChild("NadjaSpawnableHolder");
				break;
			case 2:
				t = PrefabsHolder.instance.transform.FindChild("ZafiroSpawnableHolder");
				break;
		}

		for (int i = t.childCount - 1; i >= 0; i--)
		{
			Destroy(t.GetChild(i).gameObject);
		}
	}
}
