﻿using UnityEngine;
using System;
using UnityEngine.UI;

public class CheatsController : MonoBehaviour
{
	public static CheatsController instance;

	public FakeRequests fakeRequest;
	bool infiniteEnergy = false;
	int _btnInt;

	public GameObject endPanel;
	public int biome, temple, level;
	public int guardian;

	private void Awake ()
	{
		instance = this;
	}

	public void SetBiome (int value)
	{
		biome = value;
	}

	public void SetTemple (int value)
	{
		temple = value;
	}

	public void SetLevel (int value) 
	{
		level = value;
	}

	public void SetGuardian (int value)
	{
		guardian = value;
	}

	/*
	public void TempleProgress (Single progress)
	{
		fakeRequest.desiredTempleProgression = (int)progress;
	}

	public void SetButtonInt (int btnInt)
	{
		_btnInt = btnInt;
	}
	public void SetLevelProgress (bool finished)
	{
		fakeRequest.desiredLevelProgression[_btnInt] = finished;
	}
*/
	public void InfiniteEnergy (bool isOn)
	{
		infiniteEnergy = isOn;
	}

	public void OnEnable ()
	{
		GameEventsManager.OnGameStart += SetInfiniteEnergy;
	}
	private void OnDisable ()
	{
		GameEventsManager.OnGameStart -= SetInfiniteEnergy;
	}
	public void SetInfiniteEnergy ()
	{
		GameObject.FindWithTag ("Player").GetComponent<PlayerStats>().invincible = infiniteEnergy;
	}

	public void ChooseNextChar ()
	{
		endPanel.SetActive (true);
	}
}
