﻿using UnityEngine;

public class GameEventsManager : MonoBehaviour
{
	#region Delegates
	public delegate void LevelBuildHandler ();
	public delegate void GameEventHandler ();
	public delegate void PauseEventHandler (bool paused);
	#endregion

	#region Events
	public static event LevelBuildHandler OnBuildStart;
	public static event LevelBuildHandler OnBuildEnd;
	public static event GameEventHandler OnGameStart;
	public static event GameEventHandler OnGameEnd;
	public static event PauseEventHandler OnShowDialog;
	public static event PauseEventHandler OnPause;
	#endregion

	#region Event Calls
	public static void BuildStart() { OnBuildStart(); }
	public static void BuildEnd() { OnBuildEnd(); }
	public static void GameStart() { OnGameStart (); }
	public static void GameEnd() { OnGameEnd (); }
	public static void ShowDialog(bool paused) { if (OnShowDialog != null) OnShowDialog(paused); }
	public static void GamePause(bool paused) { if (OnPause != null) OnPause (paused); }
	#endregion

	public static GameEventsManager instance;
	private void Awake () {instance = this;}

	private void OnDisable ()
	{
		if (OnBuildStart != null)
			foreach (LevelBuildHandler subscriber in OnBuildStart.GetInvocationList ())
				OnBuildStart -= subscriber;
		if (OnBuildEnd != null)
			foreach (LevelBuildHandler subscriber in OnBuildEnd.GetInvocationList ())
				OnBuildEnd -= subscriber;
		
		if (OnGameStart != null)
			foreach (GameEventHandler subscriber in OnGameStart.GetInvocationList ())
				OnGameStart -= subscriber;
		if (OnGameEnd != null)
			foreach (GameEventHandler subscriber in OnGameEnd.GetInvocationList ())
				OnGameEnd -= subscriber;

		if (OnShowDialog != null)
			foreach (PauseEventHandler subscriber in OnShowDialog.GetInvocationList ())
				OnShowDialog -= subscriber;

		if (OnPause != null)
			foreach (PauseEventHandler subscriber in OnPause.GetInvocationList ())
				OnPause -= subscriber;
	}
}
