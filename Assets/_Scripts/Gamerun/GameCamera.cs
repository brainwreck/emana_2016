﻿using UnityEngine;

public class GameCamera : MonoBehaviour
{
	public CameraFollow camScript;

	public void Init (Transform limitsHolder)
	{
		camScript.minXAndY = new Vector2(limitsHolder.FindChild ("minX").position.x + 17.7f, limitsHolder.FindChild ("minY").position.y + 10f);
		camScript.maxXAndY = new Vector2(limitsHolder.FindChild ("maxX").position.x - 17.7f, limitsHolder.FindChild ("maxY").position.y - 10f);
		camScript.playerRef = GameObject.FindGameObjectWithTag("Player").transform;
	}
}
