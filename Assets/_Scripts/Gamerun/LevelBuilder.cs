﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelBuilder : MonoBehaviour
{
	public static LevelBuilder instance;
	private void Awake ()
	{ 
		instance = this;
	}

	public LevelController levelController;
	//public Level refLevel;

	public Image loadingBg;
	public Island fakeIsland;
	public GameObject testPanel;
	private bool choseLevel;
	private TileController[] tiles;

	private void OnEnable ()
	{
		GameEventsManager.OnBuildStart += Build;
	}
	private void OnDisable ()
	{
		GameEventsManager.OnBuildStart -= Build;
	}

	private IEnumerator Start ()
	{
		//refLevel = null;
		//refLevel = MainManager.instance.loadedLevel;

		//		else refLevel = MainManager.instance.loadedLevel;
		//loadingBg.gameObject.SetActive (true);

		yield return null;

		Debug.Log ("Build Started at " + Time.timeSinceLevelLoad);

		/*actualLevel = refLevel;*/

		GameEventsManager.BuildStart ();
	}

	public void ChoseLevel ()
	{
		choseLevel = true;
		testPanel.SetActive (false);
	}
	
	public void Build () { StartCoroutine (BuildRoutine ()); }
	private IEnumerator BuildRoutine ()
	{
		Level level = GetComponent<LevelController>().level;
		//if (EncryptedPlayerPrefs.GetInt(level.sceneName + "_redFinished") == 1) level.redFinished = true;
		//if (EncryptedPlayerPrefs.GetInt(level.sceneName + "_jadeFinished") == 1) level.jadeFinished = true;
		//if (EncryptedPlayerPrefs.GetInt(level.sceneName + "_blueFinished") == 1) level.blueFinished = true;

		tiles = GetComponentsInChildren<TileController>();
		if (tiles != null)
		{
			for (int i = 0; i < tiles.Length; i++)
			{
				tiles[i].Paint(level.redFinished, level.jadeFinished, level.blueFinished);
			}
		}

		/*levelController = GetComponent<LevelController>();
		var _jasparLife = levelController.level.JasparLife;
		var _nadjaLife = levelController.level.NadjaLife;
		var _zafiroLife = levelController.level.ZafiroLife;
		levelController = GetComponent<LevelController>();

		if (levelController.level.Tiles != null)
		{
			for (int i = 0; i < levelController.level.Tiles.Count; i++)
			{
				levelController.tiles[i].Paint(levelController.level.redFinished, levelController.level.jadeFinished, levelController.level.blueFinished);
			}
		}
	
		levelController.tiles = levelController.GetComponentsInChildren<TileController> ();
		if(levelController.level.Tiles != null)
		{
			PlayerColor ownerColor = levelController.level.levelOwner;

			for (int i = 0; i < levelController.level.Tiles.Count; i++)
			{
				switch (ownerColor)
				{
					case PlayerColor.Red:
						levelController.tiles[i].Paint(true, false, false);
                        break;
					case PlayerColor.Jade:
						levelController.tiles[i].Paint(false, true, false);
						break;
					case PlayerColor.Blue:
						levelController.tiles[i].Paint(false, false, true); 
						break;
				}
			}
		}

		
		loadingBg.gameObject.SetActive (true);
		GameObject levelPrefab = Resources.Load<GameObject> (string.Format ("Levels/{0}", refLevel.prefabName));

		levelController.level = actualLevel;
		levelController.level.JasparLife = _jasparLife;
		levelController.level.NadjaLife = _nadjaLife;
		levelController.level.ZafiroLife = _zafiroLife;

		yield return null;
		*/

		Debug.Log ("Build ended at: " + Time.timeSinceLevelLoad);
		GameEventsManager.BuildEnd();
		yield break;
	}
}

