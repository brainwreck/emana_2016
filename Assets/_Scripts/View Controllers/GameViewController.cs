﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameViewController : MonoBehaviour
{
	private GameObject refHud;
	public Image manaIcon;
	public Image energyDisplay;
	private float energy, maxEnergy;
    public GameObject pausePanel;
	public Image redAvailable, jadeAvailable, blueAvailable;

	public Sprite[] manaSprites;
	public Sprite[] energySprites;

	private PlayerStats _stats;
	bool _paused;
	Vector3 maxHpScale = new Vector3(3, 3, 3);

	private void OnEnable ()
	{
		GameEventsManager.OnGameStart += Init;
		GameEventsManager.OnPause += Paused;
	}
	private void OnDisable ()
	{
		GameEventsManager.OnGameStart -= Init;
		GameEventsManager.OnPause -= Paused;
	}

	private void Init ()
	{
        _stats = GameObject.FindWithTag ("Player").GetComponent<PlayerStats>();

		manaIcon.sprite = manaSprites[(int)_stats._color];
		energyDisplay.sprite = energySprites[(int)_stats._color];

		energy = _stats.energy;
		maxEnergy = energy;

		redAvailable.enabled = false;
		jadeAvailable.enabled = false;
		blueAvailable.enabled = false;

		StopAllCoroutines();
		StartCoroutine (UIRoutine ());
	}

	private IEnumerator UIRoutine ()
	{
		float currentHp;

		while (true)
		{
			currentHp = Mathf.Clamp(energy / maxEnergy, 0, maxEnergy);
			energy = _stats.energy;
			energyDisplay.rectTransform.localScale = (maxHpScale * currentHp);

			redAvailable.enabled = _stats.overRed;
			jadeAvailable.enabled = _stats.overJade;
			blueAvailable.enabled = _stats.overBlue;

			if (Input.GetKeyDown (KeyCode.Escape) || Input.GetKeyDown(KeyCode.P))
			{
				if (!_paused)
					Pause ();
				else
				{
					Time.timeScale = 1;
					Pause ();
				}
			}

			yield return null;
		}
	}

	public void Pause ()
	{
		_paused = !_paused;
		GameEventsManager.GamePause(_paused);
	}

	public void Paused (bool paused)
	{
		pausePanel.SetActive (paused);

		Time.timeScale = paused ? 0 : 1;
	}

	public void Leave ()
	{
        Time.timeScale = 1;
        SceneManager.Instance.GoToMenu();
	}
}
