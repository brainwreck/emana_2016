﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuViewController : MonoBehaviour
{
    public static MainMenuViewController instance;

    [SerializeField]private Text dynamicLabel;
    public string defaultLabel = "Emana";

    public void Awake ()
    {
        instance = this;
    }

    public void SetLabel (string newLabel)
    {
        dynamicLabel.text = newLabel;
    }

    public void ResetLabel ()
    {
        dynamicLabel.text = defaultLabel;
    }

    #region Button Methods
    public void Play ()
    {
		if (MainManager.instance.firstTime)
			SceneManager.Instance.GoToIntro ();
        else
			SceneManager.Instance.GoToLevelSelect();
    }

    public void GoToCredits ()
    {
		// TODO
		SceneManager.Instance.FadeAndLoadScene(SceneNames.CREDITS);
	}

	public void ToggleSettings ()
    {
		SceneManager.Instance.FadeAndLoadScene(SceneNames.SETTINGS);
	}

	public void GoToExtras ()
    {
		SceneManager.Instance.FadeAndLoadScene(SceneNames.EXTRAS);
	}

	public void Exit ()
    {
        Application.Quit();
    }
    #endregion
}
