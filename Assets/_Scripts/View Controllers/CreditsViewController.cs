﻿using UnityEngine;
using System.Collections;

public class CreditsViewController : MonoBehaviour
{
	public void GoBack ()
	{
		SceneManager.Instance.FadeAndLoadScene (SceneNames.MENU);
	}
}
