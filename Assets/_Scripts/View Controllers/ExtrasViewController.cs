﻿using UnityEngine;
using System.Collections;

public class ExtrasViewController : MonoBehaviour
{
	public GameObject cardsCanvas, cutscenesCanvas;
	private bool toggle;

	public void ToggleCanvases ()
	{
		toggle = !toggle;
		cutscenesCanvas.SetActive (toggle);
		cardsCanvas.SetActive (!toggle);
	}

	public void GoBack ()
	{
		if (cardsCanvas.activeInHierarchy)
			SceneManager.Instance.FadeAndLoadScene (SceneNames.MENU);
		else
			ToggleCanvases ();
	}
}
