﻿using UnityEngine;
using UnityEngine.UI;

public class CardsViewController : MonoBehaviour
{
	public static CardsViewController instance;

	public CardButton[] cardButtons;

	public Animator bigCardAnim;
	public Image bigCardArt;
	public Text bigCardTitle;
	public Text bigCardPoem;
	public Image highlightObject;
	public CardButton currentCard;

	private void Awake ()
	{
		instance = this;

		for (int i = 0; i < cardButtons.Length; i++)
		{
			cardButtons [i].GetComponent<Button>().interactable = MainManager.instance.collectibles[i] > 0;

			if(MainManager.instance == null)
			{
				cardButtons [i].poemTitle = "Poema " + i;
				cardButtons [i].poem = string.Format ("{0}\n x\n {0}\n =\n {1}", i, i * i);
			}
		}
	}

	public void ClickedCardButton (CardButton refCard)
	{
		if(currentCard != null)
		{
			bigCardAnim.SetTrigger ("Disappear");
			if (currentCard.Equals (refCard))
			{
				currentCard = null;

				Invoke ("ResetCardValues", 1f);
				highlightObject.gameObject.SetActive(false);
				return;
			}
		}

		currentCard = refCard;
		
		bigCardAnim.SetTrigger ("Appear");
		CancelInvoke ("ResetCardValues");
		
		highlightObject.gameObject.SetActive(true);
		highlightObject.transform.SetParent (refCard.transform);
		highlightObject.rectTransform.anchoredPosition = Vector2.zero;

		// Must waiteg  tgvbm,
		bigCardArt.sprite = refCard.fullSizeArt;
		bigCardTitle.text = refCard.poemTitle;
		bigCardPoem.text = refCard.poem;
	}

	public void FlipCard ()
	{
		if (currentCard != null)
			bigCardAnim.SetTrigger (bigCardArt.gameObject.activeInHierarchy ? "Rotate" : "RotateBack");
	}

	public void ResetCardValues ()
	{
		bigCardArt.rectTransform.localEulerAngles = Vector3.zero;
		bigCardArt.gameObject.SetActive (true);
		bigCardPoem.transform.parent.localEulerAngles = Vector3.up * 180;
		bigCardPoem.transform.parent.gameObject.SetActive (false);
	}

	public void FooterButton ()
	{
		RevealBackground ();
	}

	private void RevealBackground ()
	{
		// TODO
	}
}
