﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Emana.Networking;

public class SplashScreenViewController : MonoBehaviour
{
	public	float	maxTime;

	private IEnumerator Start ()
	{
		for(float timer = 0; timer < maxTime; timer += Time.deltaTime)
		{
			yield return null;
		}

		SceneManager.Instance.FadeAndLoadScene(SceneNames.MENU);
	}
}
