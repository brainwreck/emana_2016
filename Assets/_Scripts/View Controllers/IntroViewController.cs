﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class IntroViewController : MonoBehaviour
{
	public Text text;

	private IEnumerator Start ()
	{
		EncryptedPlayerPrefs.SetInt ("firstTime", 1);
		MainManager.instance.firstTime = false;
		yield return new WaitForSeconds(22);
		SceneManager.Instance.GoToLevelSelect ();
	}

	private void Update ()
	{
		if(Input.anyKeyDown)
		{
			StopAllCoroutines ();
			SceneManager.Instance.GoToLevelSelect ();
		}
	}
}
