﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelSelectViewController : MonoBehaviour
{
    public static LevelSelectViewController instance;
	public bool test;

	[Header("Logic")]
	public TempleButton[] templeButtons;
	public LevelButton[] levelButtons;
	public int testProgress;

	[Header("Progression")]
	public GameObject[] activatedObjs;
	public GameObject[] deactivatedObjs;
	public Sprite[] templeNormalSprites, templeHighlightSprites;

	[Header("Camera")]
	public GameObject cameraObj;
	private Vector3 cameraStartPos;
	public Transform[] cameraTargets;
	private int p;

    private void Awake ()
    {
        instance = this;
		cameraStartPos = cameraObj.transform.position;
    }

	private void Start ()
	{
		if(test)
		{
			StartCoroutine (TestStart ());
			return;
		}

		p = Mathf.Clamp(MainManager.instance.myProgress, 0, 6);
		int currentTemple = Mathf.FloorToInt (p / 3);
		int levelProgress = p % 3;

		SetTempleButtons (currentTemple, levelProgress);
		SetLevelButtons (p);
		SetProgressObjects (p);
	}

	private IEnumerator TestStart ()
	{
		// CheatSheet
		// 0 1 2 3 4 5 6 7 8 9 10 11 12 p
		// 0 0 0 1 1 1 2 2 2 3 3  3  4  Floor(p / 3)
		// 0 0 0 0 1 1 1 1 2 2 2  2  3  Floor(p / 4)
		// 0 1 2 0 1 2 0 1 2 0 1  2  0  p % 3
		// 0 1 2 3 0 1 2 3 0 1 2  3  0  p % 4

		p = testProgress;

		while (p <= 12)
		{
			print (p);
			int currentTemple = Mathf.FloorToInt (p / 3);
			int levelProgress = p % 3;
			
			SetTempleButtons (currentTemple, levelProgress);
			SetLevelButtons (p);
			SetProgressObjects (p);
			yield return new WaitForSeconds (3f);
			p++;
		}
	}

	#region Visual Progression

	private void SetTempleButtons (int currentTemple, int levelProgress)
	{
		for (int i = 0; i < templeButtons.Length; i++)
		{
			templeButtons [i].gameObject.GetComponent<Button> ().interactable = i <= currentTemple;

			if (i < currentTemple)
			{
				templeButtons [i]._img.sprite = templeButtons [i].normalSprite = templeNormalSprites [3];
				templeButtons [i].highlightedSprite = templeHighlightSprites [3];
			}
			else if (i == currentTemple)
			{
				if (levelProgress == 0)
				{
					templeButtons [i]._img.sprite = templeButtons [i].normalSprite = templeNormalSprites [levelProgress];
					templeButtons [i].highlightedSprite = templeHighlightSprites [levelProgress];
				}
				else
				{
					templeButtons [i].normalSprite = templeNormalSprites [levelProgress - 1];
					templeButtons [i].highlightedSprite = templeHighlightSprites [levelProgress - 1];

					var clone = Instantiate (templeButtons [i].gameObject);
					clone.transform.SetParent (templeButtons [i].transform.parent, false);
					clone.transform.position = templeButtons [i].transform.position;
					clone.GetComponent<TempleButton> ()._img.CrossFadeAlpha (0f, 2f, false);
					Destroy (clone, 2f);

					templeButtons [i]._img.sprite = templeButtons [i].normalSprite = templeNormalSprites [levelProgress];
					templeButtons [i].highlightedSprite = templeHighlightSprites [levelProgress];
					FadeAlpha (templeButtons[i]._img, 1f);
				}
			}
		}
	}

	private void SetLevelButtons (int p)
	{
		for (int i = 0; i < levelButtons.Length; i++)
		{
			levelButtons [i]._btn.interactable = i <= p;
			levelButtons [i].fireObj.gameObject.SetActive (i <= p - 1);
			//levelButtons [i]._btn.onClick.AddListener (() => GoToLevel(i));

			if (i == p - 1)
			{
				FadeAlpha(levelButtons [i].fireObj, 1f);
			}
		}
	}

	private void SetProgressObjects (int p)
	{
		for (int i = 0; i < activatedObjs.Length; i++)
		{
			activatedObjs [i].SetActive (i <= p-1);
			if(i == p-1)
			{
				foreach (var img in activatedObjs [i].GetComponentsInChildren<Image> (true))
				{
					img.gameObject.SetActive (true);
					FadeAlpha (img, 1f);
				}
			}
		}

		for (int j = 0; j < deactivatedObjs.Length; j++)
		{
			deactivatedObjs [j].SetActive (j >= p);
			if(j == p)
			{
				FadeAlpha (deactivatedObjs [j].GetComponent<Image> (), 0f);
			}
		}
	}

	#endregion

	#region Camera Methods

	public void ResetCamera ()
	{
		iTween.MoveTo (cameraObj, new Hashtable
			{
				{ iT.MoveTo.position, cameraStartPos },
				{ iT.MoveTo.easetype, iTween.EaseType.easeInOutQuad },
				{ iT.MoveTo.time, .5f }
			});
	}

	public void GoToTarget (GameObject target)
	{
		iTween.MoveTo (cameraObj, new Hashtable
			{
				{ iT.MoveTo.position, target.transform.position },
				{ iT.MoveTo.easetype, iTween.EaseType.easeInOutQuad },
				{ iT.MoveTo.time, .5f }
			});
	}

	#endregion

	private void FadeAlpha (Image targetImg, float targetAlpha)
	{
		targetImg.color = new Color (1f, 1f, 1f, 1f - targetAlpha);
		StartCoroutine(targetImg.Fade (targetAlpha, 2f));
	}

    public void GoToLevel (int l)
    {
        SceneManager.Instance.GoToGame (l);
    }

	public void GoBack ()
	{
		SceneManager.Instance.GoToMenu ();
	}
}
