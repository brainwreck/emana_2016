﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class SettingsViewController : MonoBehaviour
{
	public static SettingsViewController instance;
	public ToggleGroup qualityToggleGroup;
	public Toggle[] qualityToggles;
	public Slider musicSlider;
	public Slider sfxSlider;

	private void Awake ()
	{
		instance = this;

		musicSlider.value = PlayerPrefs.GetFloat ("Music", .8f);
		sfxSlider.value = PlayerPrefs.GetFloat ("Sfx", 1f);
		qualityToggleGroup.SetAllTogglesOff ();
		qualityToggleGroup.NotifyToggleOn (qualityToggles [QualitySettings.GetQualityLevel () - 2]);
	}

	private void OnDestroy ()
	{
		instance = null;
	}

	public void SetQuality (int value)
	{
		if (QualitySettings.GetQualityLevel () != value)
			QualitySettings.SetQualityLevel (value, true);
	}

	public void SetMusicVolume (float value)
	{
		AudioOptions.bgmVolume = value;
		PlayerPrefs.SetFloat ("Music", value);
	}

	public void SetSfxVolume (float value)
	{
		AudioOptions.sfxVolume = value;
		PlayerPrefs.SetFloat ("Sfx", value);
	}

	public void GoBack()
	{
		SceneManager.Instance.FadeAndLoadScene(SceneNames.MENU);
	}
}

// For reference
[Serializable]
public enum QualityLevel
{
	Fastest = 0,
	Fast = 1,
	Simple = 2,
	Good = 3,
	Beautiful = 4,
	Fantastic = 5
}
