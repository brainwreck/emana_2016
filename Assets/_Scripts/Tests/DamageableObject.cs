﻿using UnityEngine;

public class DamageableObject : MonoBehaviour, IDamageable 
{
	public float hitPoints = 100;

	public void TakeDamage(float damageValue)
	{
		hitPoints -= damageValue;
		if (hitPoints <= 0) Destroy(gameObject);
	}
}
