﻿using UnityEngine;
using System.Collections;
using System;

public class TestSnake : MonoBehaviour
{
	new public Rigidbody2D rigidbody;
	public PlayerMotor _motor;
	public Transform max, min;
	public float heightFactor;

	private void Awake ()
	{
		rigidbody = GetComponentInParent<Rigidbody2D> ();
		heightFactor = max.position.y - min.position.y;
	}

	private void Update ()
	{
		float posY = Mathf.Clamp (Mathf.Sin (Time.time * 20f) * heightFactor * 10f, -heightFactor * .5f, heightFactor * .5f);
		transform.localPosition = new Vector3 (_motor.facingRight ? -.5f : .5f, posY, transform.localPosition.z);
	}
}
