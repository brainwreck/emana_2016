﻿using UnityEngine;
using System.Collections.Generic;

public class SpeakerObject : MonoBehaviour 
{
	public List<string> texts = new List<string>();
	public TextBox textBox;
	void Update () 
	{
		if (Input.anyKeyDown) textBox.Activate(texts, transform.position);
	}
}
