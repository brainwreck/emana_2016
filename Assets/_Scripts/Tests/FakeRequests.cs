﻿using UnityEngine;
using System.Collections.Generic;

public class FakeRequests : MonoBehaviour
{
	public static FakeRequests instance;
	public delegate void FakeCallback (Dictionary<string, object> fakeResponse);
	[Range(0, 15)]public int desiredTempleProgression;
	public List<bool> desiredLevelProgression;

	private void Awake () { instance = this; }

	public Island GetFakeIsland ()
	{
		Island fakeIsland = new Island ();
		fakeIsland.id = Random.Range (0, 999999);
		//fakeIsland.progress = desiredTempleProgression;
		//fakeIsland.temples [fakeIsland.progress].SetLevelProgress (desiredLevelProgression);
/*
		foreach (var level in fakeIsland.temples[fakeIsland.progress].levels)
		{
			bool r = Random.Range (0, 2) == 1;
			bool g = Random.Range (3, 5) == 4;
			bool b = Random.Range (6, 8) == 7;

			level.Tiles.ForEach(t => 
				{
					t.isRed = r;
					t.isJade = g;
					t.isBlue = b;
				});
		}*/

		return fakeIsland;
	}
}
