﻿using System;

namespace Emana.Networking.DTO
{
	public class TileDTO
	{
		public int Index { get; set; }
		public bool Red { get; set; }
		public bool Jade { get; set; }
		public bool Blue { get; set; }
	}
}

