﻿using System;

namespace Emana.Networking.DTO
{
	public class ToggleableDTO
	{
		public int id { get; set; }
		public string PrefabName { get; set; }
		public bool state { get; set; }
		public float XPos { get; set; }
		public float YPos { get; set; }
		public float ZPos { get; set; }
		public float XRot { get; set; }
		public float YRot { get; set; }
		public float ZRot { get; set; }
	}
}