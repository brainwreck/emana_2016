﻿using System.Collections;
using System.Collections.Generic;

namespace Emana.Networking.DTO
{
	public class SetLevelDTO
	{
		public int Id { get; set; }
		public string LevelName { get; set; }
		public bool RedFinished { get; set; }
		public bool BlueFinished { get; set; }
		public bool JadeFinished { get; set; }
		public List<TileDTO> Tiles { get; set; }
		public List<SpawnableDTO> Spawnables { get; set; }
		public List<ToggleableDTO> Toggleables { get; set; }
		public List<LineDTO> ZafiroLines { get; set; }
	}
}

