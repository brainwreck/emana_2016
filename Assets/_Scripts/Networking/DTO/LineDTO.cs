﻿using System.Collections.Generic;

namespace Emana.Networking.DTO
{
	public class LineDTO
	{
		public List<float> pointsX { get; set; }
		public List<float> pointsY { get; set; }
	}
}

