﻿using System;

namespace Emana.Networking.DTO
{
	public class UserDTO
	{
		public string Username { get; set; }
		public string Password { get; set; }
		public int IslandID { get; set; }
	}
}