﻿using UnityEngine;

public abstract class AbstractPlayerScript : MonoBehaviour
{
	protected PlayerController _controller;
	protected bool _paused;
	
	protected virtual void Awake ()
	{
		_controller = gameObject.GetComponent<PlayerController>();
	}
	
	public virtual void FrameUpdate () { }
	public virtual void PhysicsUpdate () { }
}

public abstract class AbstractStats : AbstractPlayerScript
{
	public PlayerColor _color;
	public virtual void SetPassives (bool red, bool jade, bool blue) {}
}

public abstract class AbstractMotor : AbstractPlayerScript
{
	public Vector2 _input;
	public float _vertical;
	public float _horizontal;
	public bool _jump;
}

public abstract class AbstractInput : AbstractPlayerScript
{
	
}
