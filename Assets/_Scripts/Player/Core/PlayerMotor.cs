﻿using UnityEngine;
using System;
using System.Collections;
using System.Xml.Linq;

public class PlayerMotor : AbstractMotor
{
	const float EPSILON = 0.00001f;

	[HeaderAttribute("Movement")]
	public	float	speedMultiplier = 1;
	public	float	maxSpeed = 4;
	public	float	acceleration = 10;
	public	bool	airControl = true;
	public	float	airAcceleration = 7.5f;
	public	float	drag = .1f;
	public	float	airDrag = 1f;
	public	Vector2	gravityEffector = new Vector2(0, -1);
	[HideInInspector]public	Vector2	baseGravity = new Vector2(9.8f, 9.8f);
	[HideInInspector]public	Vector2	baseGravityEffector;
	[HideInInspector]public	float	baseSpeedMultiplier;
	[HideInInspector]public	float	baseMaxSpeed;
	[HideInInspector]public	float	baseAcceleration;
	[HideInInspector]public	float	baseAirAcceleration;

	[HeaderAttribute("Jump")]
	public	ForceMode2D	jumpMode = ForceMode2D.Impulse;
	public	float	jumpForce = 20;
	public	bool	jumpRelease = false;
	public	bool	jumpSustain = false;
	public	float	jumpSustainForce = 5;
	public	float	jumpSustainWait = .2f;
	public	float	jumpSustainTime = .3f;
	[HideInInspector]public	float	baseJumpForce;
	public	delegate IEnumerator JumpDelegate();
	public	JumpDelegate jumpMethod;

	[HeaderAttribute("InputCorrection")]
	public	int		lateJumpFrameCorretion = 5;
	private	bool	correcting;

	[HeaderAttribute("")]
	public Transform		playerModel;
	new public Rigidbody2D	rigidbody;
	public	Transform		groundCheck;
	public	float			groundCheckHeight = .1f;
	public	LayerMask		walkableLayers;
	public	LayerMask		cornersLayer;
	private Collider2D[] 	groundHit;
	private Collider2D[]	cornerHit;
	public	bool			facingRight = true;
	public	bool			grounded, overTile;
	private bool			wallCollision, leftWalled, rightWalled;
	private	bool			onCorner, leftCorner, rightCorner;
	public	bool			_jumping, _falling;
	private Vector3 		baseScale;
	private Vector2 		_direction;
	private PlayerColor 	_color;
	private Collider2D		_collider;

	// sound
	private PlayerAudio playerAudio;

	protected override void Awake ()
	{
		base.Awake ();

		playerAudio = GetComponent<PlayerAudio>();

		jumpMethod = JumpRoutine;

		baseJumpForce = jumpForce;
		baseSpeedMultiplier = speedMultiplier;
		baseAcceleration = acceleration;
		baseAirAcceleration = airAcceleration;
		baseMaxSpeed = maxSpeed;
		baseGravityEffector = gravityEffector;
		baseScale = playerModel.localScale;
		_color = _controller._stats._color;
		_direction = Vector2.up;

		_collider = GetComponent<PolygonCollider2D> ();
		rigidbody = GetComponent<Rigidbody2D>();
		groundHit = new Collider2D[2];
		cornerHit = new Collider2D[1];
	}

	public override void PhysicsUpdate ()
	{	
		CheckCollisions();
		Move(_input, _jump);
	}

	#region Main movement loop
	public void Move (Vector2 movementInput, bool jump)
	{
		_horizontal = rigidbody.GetVector(movementInput).x;
		_vertical = rigidbody.GetVector (movementInput).y;

		Vector2 velocity = rigidbody.GetVector (rigidbody.velocity);
		_falling = velocity.y < -.1f;
		_jumping = velocity.y > .1f && transform.parent == null;

		// Flip check
		bool flipped = (Math.Abs (_horizontal - 1f) < EPSILON && velocity.x < 0) || (Math.Abs (_horizontal - -1f) < EPSILON && velocity.x > 0);
		if(flipped)
		{
			rigidbody.AddRelativeForce(new Vector2(-velocity.x, 0), ForceMode2D.Impulse);
		}
		if (Math.Abs (_horizontal - 1f) < EPSILON)
			facingRight = true;
		else if (Math.Abs (_horizontal + 1f) < EPSILON)
			facingRight = false;

		playerModel.localScale = new Vector3(facingRight ? baseScale.x : -baseScale.x, baseScale.y, baseScale.z);

		if(grounded)
		{
			// Drag correction
			rigidbody.drag = Math.Abs (_horizontal) < EPSILON ? drag * 100 : drag;

			// Moving Platform shit
			if (transform.parent != null)
			{
				Vector2 parentVelocity = transform.parent.GetComponent<Rigidbody2D>().velocity;
				rigidbody.AddRelativeForce(parentVelocity * 0.2f, ForceMode2D.Impulse);
			}

			// Move
			rigidbody.AddRelativeForce(new Vector2(_horizontal * acceleration * speedMultiplier, 0), ForceMode2D.Force);
			
			if (Mathf.Abs(rigidbody.velocity.x) > 0.1f) playerAudio.PlayEffect(EffectType.Walk);
			else playerAudio.StopEffect(EffectType.Walk);

			if (jumpForce > 1) _controller._animator.State.isJumping = jump;
			_controller._animator.State.isMoving = _horizontal > 0.1f || _horizontal < -0.1f;

			// Jump
			if(jump && !_jumping)
			{
				transform.SetParent (null);
				grounded = correcting = false;

				rigidbody.drag = airDrag;
				rigidbody.AddForce(transform.up * jumpForce, jumpMode);

				StartCoroutine(jumpMethod());
				if(jumpSustain) StartCoroutine (JumpSustainRoutine());
			}
		}
		else // Airborne
		{
			rigidbody.drag = airDrag;

			if(airControl)
			{
				rigidbody.AddRelativeForce(new Vector2(_horizontal * airAcceleration * speedMultiplier, 0), ForceMode2D.Force);
			}
		}

		// MaxSpeed check
		if(Mathf.Abs(velocity.x) > maxSpeed)
		{
			rigidbody.velocity = new Vector2(Mathf.Clamp(rigidbody.velocity.x, -maxSpeed, maxSpeed), rigidbody.velocity.y);
		}

		Vector2 gravityForce = Vector2.Scale(baseGravity, gravityEffector);
		if (!_controller._stats.dashing) rigidbody.AddForce(gravityForce);
	}
	#endregion

	#region Jump
	public IEnumerator JumpRoutine ()
	{
		if (jumpForce < 1) yield break;

		if (_controller._stats._color == PlayerColor.Blue) _controller._stats.energy -= 5;
		bool released = false;
		gravityEffector = Vector3.down * 5;
		while(!grounded)
		{
			playerAudio.PlayEffect(EffectType.Jump);
			if(jumpRelease && !released && !_falling)
			{
				rigidbody.AddForce(Vector2.Scale(baseGravity, gravityEffector), ForceMode2D.Impulse);
				released = true;
			}
			playerAudio.StopEffect(EffectType.Jump);
			yield return new WaitForFixedUpdate();
		}

		gravityEffector = -transform.up;
	}
	private IEnumerator JumpSustainRoutine ()
	{
		yield return new WaitForSeconds(jumpSustainWait);
		for(float timer = jumpSustainTime; timer > 0; timer -= Time.fixedDeltaTime)
		{
			if(_jump) rigidbody.AddForce(transform.up * jumpSustainForce, ForceMode2D.Force);
			//else yield break;
			yield return new WaitForFixedUpdate();
		}
	}
	#endregion

	#region Collisions
	private void CheckCollisions ()
	{
		// Set up reused and fixed variables
		Vector2 pointA, pointB;
		float scaleX = Mathf.Abs (transform.lossyScale.x * .5f);
		bool hit = false;
		Array.Clear (groundHit, 0, groundHit.Length);
		
		// Wall collision
		leftWalled = Physics2D.Raycast(transform.position, -transform.right, _collider.bounds.size.x * .6f);
		rightWalled = Physics2D.Raycast(transform.position, transform.right, _collider.bounds.size.x * .6f);
		wallCollision = leftWalled || rightWalled;

		// Ground collision
		pointA = new Vector2(groundCheck.position.x - scaleX, groundCheck.position.y + .1f + groundCheckHeight);
		pointB = new Vector2(groundCheck.position.x + scaleX, groundCheck.position.y - groundCheckHeight);
		Physics2D.OverlapAreaNonAlloc(pointA, pointB, groundHit, walkableLayers);
		for (int i = 0; i < groundHit.Length; i++)
		{
			if(groundHit[i] != null)
			{
				if(groundHit[i].GetComponentInChildren<TileController>() != null)
				{
					var tile = groundHit[i].GetComponentInChildren<TileController> ();
					_controller._stats.SetPassives (tile.isRed, tile.isJade, tile.isBlue);
					overTile = true;
				}
				else overTile = false;
				// Check corners
				leftCorner = !leftWalled && Physics2D.OverlapPointNonAlloc((Vector2)groundHit[i].transform.position - (Vector2)transform.right, cornerHit, walkableLayers) == 0;
				rightCorner = !rightWalled && Physics2D.OverlapPointNonAlloc((Vector2)groundHit[i].transform.position + (Vector2)transform.right, cornerHit, walkableLayers) == 0;
				onCorner = leftCorner || rightCorner;

				gravityEffector = baseGravityEffector;
				grounded = hit = true;

				if (groundHit[i].GetComponent<MovingPlatform>())
				{
					transform.SetParent (groundHit[i].transform);
				}
			}
		}

		if(!hit && grounded && !correcting)
		{
			StartCoroutine(LateJumpCorrectionRoutine());
		}

		_controller._animator.State.isGrounded = grounded;
	}
	#endregion

	private IEnumerator LateJumpCorrectionRoutine ()
	{
		correcting = true;
		transform.SetParent (null);
		for (int i = 0; i < lateJumpFrameCorretion; i++)
		{
			yield return new WaitForFixedUpdate();
		}
		grounded = correcting = false;
		gravityEffector = baseGravityEffector * 5;
	}
}
