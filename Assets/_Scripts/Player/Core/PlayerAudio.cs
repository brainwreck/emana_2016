﻿using UnityEngine;

public class PlayerAudio : MonoBehaviour {
	[SerializeField]
	private SoundBank soundbank;

	public AudioSource[] sfxSources;

	public void StopEffect(EffectType type)
	{
		AudioClip clip = null;
		soundbank.Effects.TryGetValue(type, out clip);

		for (int i = 0; i < sfxSources.Length; i++)
		{
			if (sfxSources[i].clip == clip) sfxSources[i].Stop();
		}
	}

	public void ForcePlayEffect(EffectType type, int channel)
	{
		AudioClip clip = null;
		soundbank.Effects.TryGetValue(type, out clip);

		sfxSources[channel].clip = clip;
		sfxSources[channel].Play();
	}

	public void PlayEffect(EffectType type)
	{
		int availableChannel = GetIdleChannel(sfxSources);
		if (availableChannel == -1)
		{
			return;
		}

		AudioClip clip = null;
		soundbank.Effects.TryGetValue(type, out clip);

		if (clip != null)
		{
			sfxSources[availableChannel].clip = clip;
			sfxSources[availableChannel].Play();
		}
		else
		{
			Debug.LogError("Effect \"" + type + "\" not found in Soundbank.");
		}
	}

	public void PlayEffect(EffectType type, int channel)
	{
		int availableChannel = GetIdleChannel(sfxSources, channel);
		if (availableChannel == -1)
		{
			return;
		}

		AudioClip clip = null;
		soundbank.Effects.TryGetValue(type, out clip);

		if (clip != null)
		{
			sfxSources[availableChannel].clip = clip;
			sfxSources[availableChannel].Play();
		}
		else
		{
			Debug.LogError("Effect \"" + type + "\" not found in Soundbank.");
		}
	}

	private int GetIdleChannel(AudioSource[] sources)
	{
		for (int i = 0; i < sources.Length; i++)
		{
			if (!sources[i].isPlaying)
			{
				return i;
			}
		}
		return -1;
	}

	private int GetIdleChannel(AudioSource[] sources, int channel)
	{
		if (!sources[channel].isPlaying)
			return channel;
		else
			return -1;
	}
}