using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	[Header("Player Scripts")]
	public	PlayerInput	_input;
	public	PlayerStats	_stats;
	public	PlayerMotor	_motor;
	public  PlayerAnimator _animator;

	public	bool	control = true;
	
	private void Start ()
	{
		control = false;
		GameEventsManager.OnShowDialog += OnDialog;

		StartCoroutine(FrameUpdate());
		StartCoroutine(PhysicsUpdate());
	}

	private void OnDisable()
	{
		GameEventsManager.OnShowDialog -= OnDialog;
	}

	private void OnDialog(bool paused)
	{
		control = !paused;
		if (paused) _motor.rigidbody.velocity = Vector2.zero;
		_motor.rigidbody.isKinematic = paused;
	}
	
	private IEnumerator FrameUpdate ()
	{
		GetComponent<PlayerAudio>().PlayEffect(EffectType.Spawn);
		yield return new WaitForSeconds(PlayerAnimationInfo.SPAWN_DELAY);

		GameEventsManager.GameStart();
		control = true;

		while (!_stats.Dead)
		{
			if(control)
			{
				_input.FrameUpdate ();
			}

			_motor.FrameUpdate ();
			_stats.FrameUpdate ();

			yield return null;
		}

		control = false;
	}
	
	private IEnumerator PhysicsUpdate ()
	{
		while (!_stats.Dead)
		{
			_stats.PhysicsUpdate ();
			_input.PhysicsUpdate ();
			_motor.PhysicsUpdate ();

			yield return new WaitForFixedUpdate();
		}
	}
}