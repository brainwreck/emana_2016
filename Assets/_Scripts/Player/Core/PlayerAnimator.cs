﻿using UnityEngine;

public class PlayerAnimator : MonoBehaviour {
	private Animator animator;
	public ParticleSystem jumpParticle;

	private PlayerState state;
	public PlayerState State
	{
		get
		{
			return state;
		}

		set
		{
			state = value;
		}
	}

	void Awake()
	{
		state = new PlayerState();
		animator = GetComponentInChildren<Animator>();
	}

	private void LateUpdate()
	{
		if (state.isHit)
		{
			animator.Play("Damage");
			state.isHit = false;
		}
		else if (state.isInteracting)
		{
			animator.Play("Interact");
			state.isInteracting = false;
		}

		if (state.isJumping)
		{
			jumpParticle.Play();
			animator.SetBool("isJumping", state.isJumping);

			state.isJumping = false;
		}
		else
		{
			animator.SetBool("isJumping", state.isJumping);
		}
		
		animator.SetBool("isCasting", state.isCasting);
		animator.SetBool("isGrounded", state.isGrounded);
		animator.SetBool("isMoving", state.isMoving);
	}
}
