﻿using UnityEngine;
using System.Collections;

public class PlayerStats : AbstractStats
{
	private const float EPSILON = 0.00001f;
	private	PlayerMotor _motor;
	[SerializeField]private float	_energy;
	public	float	energy {
		get { return _energy; }
		set
		{
			if (invincible)
				return;
			
			_energy = value;
			if(_energy <= 0 && !_dead)
			{
				StartCoroutine(Die());
			}
		}
	}

	public bool overRed, overJade, overBlue, invincible;
	public	bool canInteract;
	private bool _dead;
	public bool Dead { get {return _dead;} }

	[Header("Player Passives")]
	public bool canDash;
	public bool dashing;
	public bool canConsume;
	public bool	canGlide;
	public bool	isStrong;

	[Header("Player Actives")]
	public PlayerSpawnable currentSpawnable;
	private Vector2 spawnableDir;
	private GameObject spawnedObj;
	public float glideForce = 46;
	public float dashDistance = 6;
	public float redActiveWait = 5;
	public GameObject redActiveCanvas;
	public GameObject interactionCanvas;
	public float cloneCost = 10f;
	private bool usingActive;

	[Header("Feedback Prefabs")]
	public ParticleSystem deathParticle;
	public ParticleSystem zafiroSkill;
	public ParticleSystem jasparSkill;
	public ParticleSystem nadjaSkill;

	[HideInInspector]
	public string actionText;

	// sound shit
	private PlayerAudio playerAudio;

	protected override void Awake ()
	{
		base.Awake ();
		playerAudio = GetComponent<PlayerAudio>();
		_motor = GetComponent<PlayerController>()._motor;
		overRed = _color == PlayerColor.Red;
		overJade = _color == PlayerColor.Jade;
		overBlue = _color == PlayerColor.Blue;
	}

	public override void FrameUpdate ()
	{
		ApplyPassives();

		if (_controller.control)
		{
			if (Input.GetButtonDown("UseActive")) UseActive();
			else if (_color == PlayerColor.Blue && Input.GetButtonDown("Jump")) UseActive();
			else if (Input.GetButtonDown("Interact") && currentSpawnable != null) UseSpawnable();
		}
	}

	private IEnumerator Die ()
	{ 
		_dead = true;
		_controller.control = false;
		_motor._horizontal = 0;
		_motor._jump = false;
		playerAudio.PlayEffect(EffectType.Death);
		redActiveCanvas.SetActive (false);
		canDash = canConsume = canGlide = false;
		
		Time.timeScale = 1;
		Instantiate (deathParticle, transform.position, transform.rotation);
		// uncomment for char selection
		//FindObjectOfType<CheatsController>().ChooseNextChar();

		GameEventsManager.GameEnd ();
		StopAllCoroutines ();
		yield break;
	}

	#region Actives
	private void UseActive ()
	{
		// use Player's active
		if (!usingActive)
		{
			switch (_color)
			{
			case PlayerColor.Red:
				playerAudio.ForcePlayEffect(EffectType.SkillJaspar, 0);
				StartCoroutine (RedActiveRoutine (Vector3.zero));
				break;
			case PlayerColor.Jade:
				if(_motor.grounded) StartCoroutine (JadeActiveRoutine ());
				break;
 			case PlayerColor.Blue:
				playerAudio.ForcePlayEffect(EffectType.SkillZafiro, 0);
				StartCoroutine (BlueActiveRoutine ());
				break;
			}
		}
	}

	private void UseSpawnable()
	{
		// use Spawnable's active
		if (currentSpawnable != null)
		{
			if (currentSpawnable.color == PlayerColor.Red && _color != PlayerColor.Red)
			{
				playerAudio.ForcePlayEffect(EffectType.SkillJaspar, 0);
				StartCoroutine(RedActiveRoutine(spawnableDir));
				return;
			}
			if (currentSpawnable.color == PlayerColor.Blue && _color != PlayerColor.Blue)
			{
				playerAudio.ForcePlayEffect(EffectType.SkillZafiro, 0);
				StartCoroutine(BlueActiveRoutine());
				return;
			}
			if (currentSpawnable.color == PlayerColor.Jade)
			{
				playerAudio.ForcePlayEffect(EffectType.SkillNadja, 0);
				ConsumeClone();
				return;
			}
		}
	}

	private void SetCurrentSpawnable (PlayerSpawnable spawnable)
	{
		switch (spawnable.color)
		{
		case PlayerColor.Red:
			canDash = true;
			actionText = _color == spawnable.color ? "" : "Arrancada";
			break;
		case PlayerColor.Jade:
			canConsume = true;
			actionText = _color == spawnable.color ? "" : "Escalar";
            break;
		case PlayerColor.Blue:
			canGlide = true;
			actionText = _color == spawnable.color ? "" : "Planar";
            break;
		}

		currentSpawnable = spawnable;
	}

	private void ClearCurrentSpawnable (PlayerSpawnable spawnable)
	{
		if (spawnable.color == _color) return;

		switch (spawnable.color)
		{
		case PlayerColor.Red:
			canDash = false;
			break;
		case PlayerColor.Jade:
			canConsume = false;
			break;
		case PlayerColor.Blue:
			canGlide = false;
			break;
		}

		actionText = "";
		currentSpawnable = null;
	}

	private IEnumerator RedActiveRoutine (Vector3 preDir)
	{
		usingActive = true;
		_controller.control = false;
		Vector3 oldDir = Vector3.zero;
		Time.timeScale = .05f;
		_motor.gravityEffector = Vector2.zero;
		yield return new WaitForSeconds (.05f * Time.timeScale);

		Vector3 dir = Vector3.zero;
		// 2 right  1 left  0 up
		bool[] usedDirs = new bool[] {false, false, false};
		if (!_motor.grounded && !_motor._jumping)
		{ 
			usedDirs[0] = true;
			redActiveCanvas.transform.FindChild("UpArrow").gameObject.SetActive(false);
		}

		// From placed orbs
		if(preDir != Vector3.zero)
		{
			dashing = true;
			jasparSkill.Play();
			Time.timeScale = 1;
			dir = preDir;
			spawnableDir = Vector3.zero;
			Vector3 targetPos = transform.position + dir * dashDistance;
			Vector3 deltaPos = targetPos - transform.position;
			for(float f = 0; f < .5f; f += Time.fixedDeltaTime)
			{
				_motor.rigidbody.MovePosition (transform.position + deltaPos * Time.fixedDeltaTime * 2f);
				yield return new WaitForFixedUpdate ();
			}
			_motor.rigidbody.velocity = Vector2.zero;

			if (currentSpawnable != null && currentSpawnable.color == PlayerColor.Red && Input.GetButton("Interact"))
			{
				StartCoroutine(RedActiveRoutine (spawnableDir));
				yield break;
			}

			goto Finish;
		}

		CheckInput:
		_motor.rigidbody.velocity = Vector2.zero;
		_motor.rigidbody.isKinematic = true;
		redActiveCanvas.SetActive (true);

		while (Input.GetButton("UseActive"))
		{
			if ((Mathf.Abs (Input.GetAxis ("Horizontal") - 1) < EPSILON) && !usedDirs[2])
			{
				dir = Vector3.right;
				if (!_motor.grounded)
				{
					usedDirs[2] = true;
					redActiveCanvas.transform.FindChild("RightArrow").gameObject.SetActive(false);
				}
				break;
			}
			else if ((Mathf.Abs (Input.GetAxis ("Horizontal") + 1) < EPSILON) && !usedDirs[1])
			{
				dir = -Vector3.right;
				if (!_motor.grounded)
				{
					usedDirs[1] = true;
					redActiveCanvas.transform.FindChild("LeftArrow").gameObject.SetActive(false);
				}
				break;
			}
			else if ((Mathf.Abs(Input.GetAxis("Vertical") - 1) < EPSILON) && !usedDirs[0])
			{
				dir = Vector3.up;
				usedDirs[0] = true;
				redActiveCanvas.transform.FindChild("UpArrow").gameObject.SetActive(false);
				break;
			}

			yield return new WaitForEndOfFrame();
		}

		Time.timeScale = 1;

		if(dir != Vector3.zero)
		{
			dashing = true;
			jasparSkill.Play();
			_controller._animator.State.isCasting = true;
			GameObject orb = Instantiate (PrefabsHolder.instance.JasparOrb, transform.position, Quaternion.LookRotation (dir)) as GameObject;
			orb.transform.parent = PrefabsHolder.instance.transform.FindChild("JasparSpawnableHolder");
			redActiveCanvas.SetActive (false);
			oldDir = dir;
			_motor.rigidbody.isKinematic = false;
			Vector3 targetPos = transform.position + dir * dashDistance;
			Vector3 deltaPos = targetPos - transform.position;
			energy -= 5;
			for(float f = 0; f < .5f; f += Time.fixedDeltaTime)
			{
				_motor.rigidbody.MovePosition (transform.position + deltaPos * Time.fixedDeltaTime * 2f);
				yield return new WaitForFixedUpdate ();
			}
			dashing = false;
			_motor.rigidbody.velocity = Vector2.zero;
			Time.timeScale = .05f;
			dir = Vector3.zero;
			_controller._animator.State.isCasting = false;
			goto CheckInput;
		}

		Finish:
		yield return null;
		Time.timeScale = 1;
		dashing = false;
		_motor.rigidbody.isKinematic = false;
		_controller.control = true;
		redActiveCanvas.transform.FindChild("LeftArrow").gameObject.SetActive(true);
		redActiveCanvas.transform.FindChild("RightArrow").gameObject.SetActive(true);
		redActiveCanvas.transform.FindChild("UpArrow").gameObject.SetActive(true);
		redActiveCanvas.SetActive (false);
		usingActive = false;
		_motor.gravityEffector = _motor.baseGravityEffector * 5;
	}

	private IEnumerator BlueActiveRoutine ()
	{
		usingActive = true;
		GameObject trail = null;

		if (_color == PlayerColor.Blue)
		{
			trail = Instantiate(PrefabsHolder.instance.ZafiroTrail, transform.position, Quaternion.identity) as GameObject;
			trail.transform.SetParent(transform);
			trail.GetComponent<ZafiroTrail>().Init();

			_controller._animator.State.isCasting = true;
			while ((Input.GetKey (KeyCode.Space)) && canGlide && !_motor.grounded)
			{
				if (_motor._falling)
				{
					_motor.rigidbody.AddForce (transform.up * glideForce, ForceMode2D.Force);
					energy -= Time.fixedDeltaTime * 5;
				}
				if (!_controller.control)
					break;
				yield return new WaitForFixedUpdate();
			}
		}
		else
		{
			while (!(Input.GetAxis("Vertical") + 1 < EPSILON) && currentSpawnable != null)
			{
				if (_motor._falling)
				{
					_motor.rigidbody.AddForce(transform.up * glideForce, ForceMode2D.Force);
				}
				if (!_controller.control)
					break;
				yield return new WaitForFixedUpdate();
			}
		}

		if (!zafiroSkill.isPlaying) zafiroSkill.Play();

		if (_color == PlayerColor.Blue)
		{
			trail.transform.SetParent(PrefabsHolder.instance.transform.FindChild("ZafiroSpawnableHolder"));
			trail.GetComponent<ZafiroTrail>().Stop();
		}

		_controller._animator.State.isCasting = false;
		zafiroSkill.Stop();
		usingActive = false;
	}

	private IEnumerator	JadeActiveRoutine ()
	{
		if (usingActive) yield break;
		usingActive = true;

		Vector3 offset = new Vector3(0, 0.5f);
		_controller._animator.State.isCasting = true;
		energy -= cloneCost;

		yield return new WaitForSeconds(PlayerAnimationInfo.NADJA_CAST_DELAY);

		playerAudio.ForcePlayEffect(EffectType.SkillNadja, 0);
		nadjaSkill.Play();
		GameObject clone = Instantiate (PrefabsHolder.instance.NadjaClone, transform.position - offset, PrefabsHolder.instance.NadjaClone.transform.rotation) as GameObject;
		clone.transform.parent = PrefabsHolder.instance.transform.FindChild("NadjaSpawnableHolder");
		transform.position = clone.transform.FindChild ("Top").position;
		_controller._animator.State.isCasting = false;

		usingActive = false;
	}

	private void ConsumeClone ()
	{
		usingActive = true;

		if (spawnedObj != null) spawnedObj.GetComponent<NadjaClone> ().GetConsumed ();
		currentSpawnable = null;
		spawnedObj = null;

		usingActive = false;
	}
	#endregion

	#region Passives
	public override void SetPassives (bool red, bool jade, bool blue)
	{
		base.SetPassives (red, jade, blue);
		overRed = (_color == PlayerColor.Red || red);
		overJade = (_color == PlayerColor.Jade || jade);
		overBlue = (_color == PlayerColor.Blue || blue);
	}

	private void ApplyPassives ()
	{
		// Red
		if(overRed)
		{
			_motor.speedMultiplier = 2;
			_motor.maxSpeed = 10;
		}
		else
		{
			_motor.speedMultiplier = _motor.baseSpeedMultiplier;
			_motor.maxSpeed = _motor.baseMaxSpeed;
		}

		// Jade
		isStrong = overJade;

		// Blue
		_motor.jumpForce = overBlue ? _motor.baseJumpForce : 0;
	}
	#endregion

	#region Collisions
	private void OnCollisionEnter2D (Collision2D hit)
	{
		if(hit.gameObject.GetComponentInChildren<TileController>() != null)
		{
			TileController tile = hit.gameObject.GetComponentInChildren<TileController>();
			switch (_color)
			{
			case PlayerColor.Red:
				if (!tile.isRed)
				{
					energy -= .5f;
					tile.Paint (_color);
				}
				break;
			case PlayerColor.Jade:
				if (!tile.isJade)
				{
					energy -= .5f;
					tile.Paint (_color);
				}
				break;
			case PlayerColor.Blue:
				if (!tile.isBlue)
				{
					energy -= .5f;
					tile.Paint (_color);
				}
				break;
			}
		}
		if (hit.gameObject.GetComponent<IDestructible>() != null)
		{
			hit.gameObject.GetComponent<IDestructible> ().Crush (_color);
		}
		if(hit.gameObject.GetComponent<Bullet>() != null)
		{
			if (dashing)
				return;

			var b = hit.gameObject.GetComponent<Bullet> ();
			energy -= b.damage;
			_motor.rigidbody.AddForce (b.GetComponent<Rigidbody2D>().velocity, ForceMode2D.Impulse);
		}
		if(hit.gameObject.name.Contains ("Hero_Spawn") && !hit.gameObject.Equals (LevelBuilder.instance.levelController.spawnPoint.gameObject))
		{
			// TODO: Abomin Statue thing
			LevelBuilder.instance.levelController.success = true;
			GameEventsManager.GameEnd ();
		}
	}

	private void OnCollisionStay2D (Collision2D other)
	{
		if (isStrong && other.gameObject.GetComponent<IPushable>() != null && !usingActive)
		{
			if (currentSpawnable != null && currentSpawnable.color == PlayerColor.Jade) 
				other.gameObject.GetComponent<IPushable>().Push((_motor.facingRight ? Vector2.right : Vector2.left) * 5);
		}
	}

	private void OnTriggerEnter2D (Collider2D other)
	{
		if (other.GetComponent<PlayerSpawnable>() != null)
		{
			SetCurrentSpawnable (other.GetComponent<PlayerSpawnable>());

			if(other.GetComponent<PlayerSpawnable>().color == PlayerColor.Red)
			{
				spawnableDir = other.transform.forward;
			}
			else if (other.GetComponent<PlayerSpawnable>().color == PlayerColor.Jade)
			{
				spawnableDir = other.transform.up;
				spawnedObj = other.gameObject;
			}
		}

		if(other.GetComponent<IHittable>() != null)
			other.GetComponent<IHittable> ().Hit();
	}
    private void OnTriggerStay2D (Collider2D other)
    {
		PlayerSpawnable spawnable = other.GetComponent<PlayerSpawnable>();
        if (spawnable != null)
		{
			if (spawnable.color == PlayerColor.Blue && _color != PlayerColor.Blue && !usingActive)
			{
				SetCurrentSpawnable(other.GetComponent<PlayerSpawnable>());
				StartCoroutine(BlueActiveRoutine());
			}
		}
		if (other.GetComponent<IInteractable>() != null)
        {
			actionText = "Interagir";
			interactionCanvas.SetActive(true);
			if (Input.GetButtonDown("Interact"))
			{
				playerAudio.ForcePlayEffect(EffectType.Interact, 0);
				other.GetComponent<IInteractable>().OnInteract(this);
				_controller._animator.State.isInteracting = true;
			}
        }
    }
	private void OnTriggerExit2D (Collider2D other)
	{
		if(other.GetComponent<PlayerSpawnable>() != null)
		{
			ClearCurrentSpawnable (other.GetComponent<PlayerSpawnable>());
		}
		if (other.GetComponent<IInteractable>() != null)
		{
			actionText = "";
			interactionCanvas.SetActive(false);
        }
	}
	#endregion
}

public enum PlayerColor
{
	Red,
	Jade,
	Blue
}
