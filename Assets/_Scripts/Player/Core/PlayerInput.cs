﻿using UnityEngine;
using System.Collections;

public class PlayerInput : AbstractInput
{
	private	float	_horizontal, _vertical;
	private	bool	_jump;
    private bool    _interact;
	private	bool	control = true;
	
	[Header("InputCorrection")]
	public	int		earlyJumpFrameCorrection = 3;
	
	public override void FrameUpdate ()
	{
		if(control)
		{
			if(Input.GetButtonDown("Jump") && !_jump) StartCoroutine(JumpInput());
			_horizontal = Input.GetAxis("Horizontal");
			_vertical = Input.GetAxis ("Vertical");
            //_interact = Input.GetButton("Interact");
		}

		_controller._motor._input = new Vector2 (_horizontal, _vertical);
		_controller._motor._jump = _jump;
	}
	
	private IEnumerator JumpInput ()
	{
		_jump = true;
		for (int i = 0; i < earlyJumpFrameCorrection; i++) yield return null;
		_jump = false;
	}
	
	public void Stop ()
	{
		control = false;
	}
	public void Resume ()
	{
		control = true;
	}
}
