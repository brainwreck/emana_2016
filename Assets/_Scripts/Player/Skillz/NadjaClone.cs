﻿using UnityEngine;
using System.Collections;

public class NadjaClone : MonoBehaviour
{
	public LayerMask cloneLayer;
	private bool canConsume = true;
	public float healAmount = 10f;

	public bool stacked;
	private int indexInStack;

	private Vector3 offset = new Vector3(0, 1f);

	public int IndexInStack
	{
		get
		{
			return indexInStack;
		}

		set
		{
			if (value > 3)
			{
				GetConsumed();
			}

			indexInStack = value;
		}
	}

	private void OnEnable()
	{
		canConsume = true;
	}

	public void GetConsumed () { if(canConsume) StartCoroutine (GetConsumedRoutine ()); }
	public IEnumerator GetConsumedRoutine ()
	{
		canConsume = false;
		for(int frame = 0; frame < 5; frame++) yield return null;

		RaycastHit2D[] hit;
		hit = Physics2D.RaycastAll (transform.position, transform.up, 1.5f, cloneLayer);
		for (int i = 0; i < hit.Length; i++)
		{
			if(hit[i].collider.gameObject.GetComponent<NadjaClone>() != null)
			{
				hit[i].collider.gameObject.GetComponent<NadjaClone>().GetConsumed ();
			}
		}
		
		GameObject.FindWithTag ("Player").GetComponent<PlayerStats> ().energy += healAmount;
		Destroy(gameObject);

		yield return null;
	}
}

