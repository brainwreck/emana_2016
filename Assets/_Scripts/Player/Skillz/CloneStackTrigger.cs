﻿using UnityEngine;

public class CloneStackTrigger : MonoBehaviour 
{
	public NadjaClone parentClone;

	private void OnTriggerEnter2D(Collider2D hit)
	{
		if (hit.gameObject.GetComponent<NadjaClone>() != null && transform.position.y > hit.transform.position.y)
		{
			parentClone.stacked = true;
			parentClone.IndexInStack = hit.GetComponent<NadjaClone>().IndexInStack + 1;
			
			Stack(hit.transform);
		}
	}

	private void OnTriggerExit2D(Collider2D hit)
	{
		if (hit.gameObject.GetComponent<NadjaClone>() != null && transform.position.y > hit.transform.position.y)
		{
			parentClone.stacked = false;
			parentClone.IndexInStack = 0;

			Stack(null);
		}
	}

	private void Stack(Transform parent)
	{
		if (parent != null) parentClone.transform.position = new Vector2(parent.transform.position.x, parent.transform.position.y + 1.5f);
		parentClone.transform.parent = parent;
		parentClone.GetComponent<Rigidbody2D>().isKinematic = parent != null;
	}
}
