﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZafiroTrail : MonoBehaviour, ISpawnable
{
	#region ISpawnable implementation
	public float XPos { get; set; }
	public float YPos { get; set; }
	public float ZPos { get; set; }
	public float XRot { get; set; }
	public float YRot { get; set; }
	public float ZRot { get; set; }
	#endregion

	public LineRenderer line;
	[SerializeField]
	private GameObject colliderPrefab;
	private bool stop;

	private void OnEnable ()
	{
		GameEventsManager.OnGameEnd += Stop;
	}
	private void OnDisable ()
	{
		GameEventsManager.OnGameEnd -= Stop;
	}

	public void Init ()
	{
		stop = false;
		StartCoroutine (TrailRoutine ());
	}
	public void Stop ()
	{
		GameEventsManager.OnGameEnd -= Stop;
		stop = true;
	}
	private IEnumerator TrailRoutine ()
	{
		int i = 0;
		List<GameObject> colliders = new List<GameObject>();

		while (!stop)
		{
			Vector3 startPos = transform.position;
	        line.SetVertexCount (i+1);
			line.SetPosition (i, transform.position);
			i++;

			yield return new WaitForSeconds (.25f);

			colliders.Add(GenerateCollider(startPos, transform.position));
		}

		Destroy(colliders[colliders.Count - 1]);
		colliders.RemoveAt(colliders.Count - 1);
		for (int j = 0; j < colliders.Count; j++)
		{
			colliders[j].transform.SetParent(transform);
		}

		yield return null;
	}

	private GameObject GenerateCollider(Vector3 startPos, Vector3 endPos)
	{
		Vector3 difference = endPos - startPos;
		GameObject coll = Instantiate(colliderPrefab, startPos, Quaternion.identity) as GameObject;
		Vector3 scale = new Vector3(Vector3.Distance(startPos, endPos), 1, 1);
		coll.transform.localScale = scale;
		coll.transform.localRotation = Quaternion.Euler(0, 0, Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg);

		return coll;
	}
}
