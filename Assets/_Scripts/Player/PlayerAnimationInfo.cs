﻿public static class PlayerAnimationInfo 
{
	#region Common Values
	public const float SPAWN_DELAY = 4f;
	public const float DEATH_DELAY = 4f;
	#endregion

	#region Nadja Values
	public const float NADJA_CAST_TOTAL = 1f;
	public const float NADJA_CAST_DELAY = 0.66f;
	public const float NADJA_HIT_TOTAL = 1f;
	#endregion

	#region Zafiro Values
	#endregion

	#region Jaspar Values
	#endregion
}
