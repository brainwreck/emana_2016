﻿using UnityEngine;

public class Collectible : MonoBehaviour 
{
	public int id;

	[Multiline]
	public string feedbackText;

	void Start () 
	{
		if (MainManager.instance.collectibles[id] == 1)
		{
			Destroy(gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.GetComponent<PlayerStats>() != null)
		{
			TextBox.instance.Activate(feedbackText, transform.position);

			MainManager.instance.collectibles[id] = 1;
			MainManager.instance.SaveCollectibles();

			// TODO: sound + anim feedback before destruction
			Destroy(gameObject);
		}
	}
}
