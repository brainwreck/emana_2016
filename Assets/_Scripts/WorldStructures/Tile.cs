﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Tile
{
	public int id;
	public bool isRed;
	public bool isJade;
	public bool isBlue;
}
