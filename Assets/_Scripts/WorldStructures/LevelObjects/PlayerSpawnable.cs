﻿using UnityEngine;

public class PlayerSpawnable : MonoBehaviour, ISpawnable
{
	public float XPos { get; set; }
	public float YPos { get; set; }
	public float ZPos { get; set; }
	public float XRot { get; set; }
	public float YRot { get; set; }
	public float ZRot { get; set; }

	public PlayerColor color;
}
