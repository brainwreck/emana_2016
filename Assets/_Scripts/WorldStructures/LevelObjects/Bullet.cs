﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour, IHittable
{
	public float damage = 10;
	private float force = 10;
	public float maxDistance = 99;
	private Vector3 startPos;

	public void Init ()
	{
		startPos = transform.position;
		GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		GetComponent<Rigidbody2D>().AddForce(transform.up * force, ForceMode2D.Impulse);
	}

	private void FixedUpdate ()
	{
		if (Vector2.Distance(startPos, transform.position) > maxDistance)
		{
			Hit();
		}
	}

	public void Hit () {Hit (transform.position);}
	public void Hit (Vector3 pos)
	{
		var bulletExplosion = PrefabsHolder.instance.bulletExplosionPool.GetPooledObject ();
		bulletExplosion.transform.position = pos;
		bulletExplosion.transform.rotation = transform.rotation;
		bulletExplosion.SetActive (true);

		gameObject.SetActive (false);
	}

	private void OnCollisionEnter2D (Collision2D hit)
	{
		Hit();
	}
}
