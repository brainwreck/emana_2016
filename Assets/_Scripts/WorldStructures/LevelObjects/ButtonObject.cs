﻿using UnityEngine;
using System.Collections;

public class ButtonObject : MonoBehaviour
{
	private void OnCollisionEnter2D (Collision2D hit)
	{
		if(hit.gameObject.CompareTag ("Player") || hit.gameObject.CompareTag ("Pushable"))
		{
			GetComponent<Toggleable> ().TurnOn ();
		}
	}
}
