﻿using UnityEngine;
using System.Collections;

public class FeedbackObject : Colorable
{
	public	Material materialToChange, newMaterial;
	public	PlayerColor affectedByColor;
	public	Animation anim;

	#region IColorProgress implementation

	public void Paint (bool paintRed, bool paintJade, bool paintBlue)
	{
		if(affectedByColor == PlayerColor.Red && paintRed && !isRed)
		{
			isRed = true;
			Activate ();
		}

		if(affectedByColor == PlayerColor.Jade && paintJade && !isJade)
		{
			isJade = true;
			Activate ();
		}

		if(affectedByColor == PlayerColor.Blue && paintBlue && !isBlue)
		{
			isBlue = true;
			Activate ();
		}
	}	

	public bool isRed { get; set; }
	public bool isJade { get; set; }
	public bool isBlue { get; set; }

	#endregion

	private void OnEnable ()
	{
		GameEventsManager.OnBuildEnd += Init;
	}

	private void Init ()
	{
		GameEventsManager.OnBuildEnd -= Init;

		if (affectedByColor == PlayerColor.Red && isRed)
			Activate ();
		else if (affectedByColor == PlayerColor.Jade && isJade)
			Activate ();
		else if (affectedByColor == PlayerColor.Blue && isBlue)
			Activate ();
	}

	private void Activate ()
	{
		if(anim != null)
		{
			anim.Play ();
		}

		if(materialToChange != null)
		{
			materialToChange = newMaterial;
		}
	}
}

