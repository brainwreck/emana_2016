﻿using UnityEngine;

public class Water : MonoBehaviour
{
	private Material mat;

	private void Awake ()
	{
		mat = GetComponent<Renderer> ().material;
	}

	private void Update ()
	{
		mat.mainTextureOffset += new Vector2 (-.1f, .2f) * Time.deltaTime;
	}

	private void OnCollisionEnter2D(Collision2D hit)
	{
		if (hit.gameObject.GetComponent<PlayerStats>() != null)
		{
			hit.gameObject.GetComponent<PlayerStats>().energy -= 10000f;
		}
	}
}
