﻿using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour, IActivated
{
	public Transform muzzle;
	public float firstShotOffset;
	public float _maxDistance = 20;
	public float fireDelay = 1.2f;
	public float force = 10;
	public bool buttonActivated;

	private AudioSource source;

	private void Start ()
	{
		source = GetComponent<AudioSource>();
		if (!buttonActivated) InvokeRepeating("Fire", firstShotOffset, fireDelay);
	}

	private void Fire ()
	{
		source.Play();
		GameObject bullet;
		bullet = PrefabsHolder.instance.bulletPool.GetPooledObject ();	
		bullet.transform.rotation = muzzle.rotation;
		bullet.transform.position = muzzle.position;
		bullet.GetComponent<Bullet>().maxDistance = _maxDistance;
		bullet.GetComponent<Bullet>().Init();
		bullet.SetActive (true);
	}

	#region IActivated implementation

	public void OnActivate ()
	{
		InvokeRepeating("Fire", firstShotOffset, fireDelay);
	}

	public void OnDeactivate ()
	{
		CancelInvoke("Fire");
	}

	#endregion
}
