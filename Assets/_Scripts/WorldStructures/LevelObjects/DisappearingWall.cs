﻿using UnityEngine;
using System.Collections;

public class DisappearingWall : MonoBehaviour, IActivated
{
	public void OnActivate()
	{
		GetComponent<Collider2D>().enabled = false;
		GetComponent<Renderer>().enabled = false;
	}

	public void OnDeactivate(){}
}
