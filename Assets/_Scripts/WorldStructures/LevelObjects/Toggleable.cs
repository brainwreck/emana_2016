﻿using UnityEngine;
using System.Collections;
using System;

public class Toggleable : MonoBehaviour, IToggle
{
	[SerializeField]
	private bool _state;
	[SerializeField]
	private bool allowMultipleActivation = false;
	public bool state { get{ return _state; } set{ _state = value;} }

	/// <summary>
	/// Objects that implement IActivated interface; 
	/// </summary>
	public GameObject[] affectedObjects;

	private void Start ()
	{
		if (_state)
		{
			foreach (var obj in affectedObjects)
			{
				if(obj.GetComponent<IActivated>() != null)
					obj.GetComponent<IActivated>().OnActivate();
				if (obj.GetComponent<MultiActivated>() != null) 
					obj.GetComponent<MultiActivated>().ChangeState (this);
			}
		}
	}

	public void TurnOn ()
	{
		if (_state && !allowMultipleActivation)
			return;
		_state = true;

		foreach (var obj in affectedObjects)
		{
			if(obj.GetComponent<IActivated>() != null)
				obj.GetComponent<IActivated>().OnActivate();
			if (obj.GetComponent<MultiActivated>() != null) 
				obj.GetComponent<MultiActivated>().ChangeState (this);
		}
	}

	public void TurnOff ()
	{
		if (!_state)
			return;
		_state = false;

		foreach (var obj in affectedObjects)
		{
			if(obj.GetComponent<IActivated>() != null)
				obj.GetComponent<IActivated>().OnDeactivate();
			if (obj.GetComponent<MultiActivated>() != null) 
				obj.GetComponent<MultiActivated>().ChangeState (this);
		}
	}
}
