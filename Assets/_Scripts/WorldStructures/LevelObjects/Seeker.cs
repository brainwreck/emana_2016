﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Seeker : MonoBehaviour
{
	new private Rigidbody2D rigidbody;
	private	Transform	target;
	private	bool		seek;
	[SerializeField]
	private float		range = 50;
	[SerializeField]
	public float 		speed = 10;
	[SerializeField]
	public float 		maxSpeed = 4;
	private	Vector2		dir;

	[SerializeField]
	private float		damage = 5;
	[SerializeField]
	private float		attackRate = 3;

	#region Initialization
	private void OnEnable ()
	{
		GameEventsManager.OnGameStart += SetTarget;
		GameEventsManager.OnGameEnd += StopSeeking;
		GameEventsManager.OnPause += Pause;
		GameEventsManager.OnShowDialog += Pause;
	}
	private void OnDisable ()
	{
		GameEventsManager.OnGameStart -= SetTarget;
		GameEventsManager.OnGameEnd -= StopSeeking;
		GameEventsManager.OnPause -= Pause;
		GameEventsManager.OnShowDialog -= Pause;
	}

	private void Start ()
	{
		rigidbody = GetComponent<Rigidbody2D> ();
	}

	private void SetTarget()
	{
		target = GameObject.FindWithTag ("Player").transform;
		if (Vector2.Distance(transform.position, target.position) > range) 
			Invoke("SetTarget", 2f);
		else 
			StartSeeking();
	}
	#endregion

	private void Pause (bool paused)
	{
		if (target != null) seek = !paused;
	}

	public void StartSeeking ()
	{
		seek = true;
	}
	public void StopSeeking ()
	{
		seek = false;
	}

	private void FixedUpdate ()
	{
		if(seek)
		{
			if (rigidbody.velocity.sqrMagnitude > maxSpeed * maxSpeed)
				return;
			
			dir = (Vector2)target.position - (Vector2)transform.position;
			rigidbody.AddForce (dir.normalized * speed);
		}
	}

	private void Attack()
	{
		if (seek)
			target.GetComponent<PlayerStats>().energy -= damage;
	}

	private void OnTriggerEnter2D (Collider2D other)
	{
		if (other.transform.Equals(target))
		{
			InvokeRepeating("Attack", attackRate * 0.5f, attackRate);
		}
	}

	private void OnTriggerExit2D (Collider2D other)
	{
		if (other.transform.Equals(target))
		{
			CancelInvoke("Attack");
		}
	}
}
