﻿using UnityEngine;

public class EnemySpawn : MonoBehaviour, IActivated
{
	private bool hasSpawned = false;

	#region IActivated implementation
	public void OnActivate ()
	{
		if (!hasSpawned) Spawn ();
	}
	public void OnDeactivate () {}
	#endregion

	public void Spawn ()
	{
		hasSpawned = true;
		Instantiate (PrefabsHolder.instance.seekerPrefab, transform.position, transform.rotation);
	}
}
