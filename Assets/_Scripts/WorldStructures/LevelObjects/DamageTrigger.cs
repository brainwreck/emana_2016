﻿using UnityEngine;
using System.Collections;

public class DamageTrigger : MonoBehaviour {
	[SerializeField]
	private float damageRate;
	[SerializeField]
	private float damageValue;

	private PlayerStats target;

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Player") && target == null)
		{
			target = other.GetComponent<PlayerStats>();
			InvokeRepeating("Damage", 0, damageRate);
		}
	}

	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.CompareTag("Player"))
		{
			target = null;
			CancelInvoke("Damage");
		}
	}

	private void Damage()
	{
		target.energy -= damageValue;
	}
}
