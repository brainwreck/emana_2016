﻿using UnityEngine;
using System.Collections;

public class Colorable : MonoBehaviour, IColorProgress
{
	public bool isRed { get; set; }
	public bool isJade { get; set; }
	public bool isBlue { get; set; }

	public void Paint (bool paintRed, bool paintJade, bool paintBlue)
	{
		if(isRed != paintRed) isRed = paintRed;
		if(isJade != paintJade) isJade = paintJade;
		if(isBlue != paintBlue) isBlue = paintBlue;
	}
}

