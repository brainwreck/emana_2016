﻿using UnityEngine;
using System.Collections;

public class SimpleTrigger : MonoBehaviour
{
	public GameObject[] affectedObjects;

	private void OnTriggerEnter2D (Collider2D other)
	{
		if (other.GetComponent<PlayerStats>() != null)
		{
			for (int i = 0; i < affectedObjects.Length; i++)
			{
				affectedObjects [i].GetComponent<IActivated> ().OnActivate ();
			}
		}
	}
}
