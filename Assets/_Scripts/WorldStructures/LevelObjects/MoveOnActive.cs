﻿using UnityEngine;
using System.Collections;

public class MoveOnActive : MonoBehaviour, IActivated, ISpawnable, IChange
{
	public float XPos { get; set; }
	public float YPos { get; set; }
	public float ZPos { get; set; }
	public float XRot { get; set; }
	public float YRot { get; set; }
	public float ZRot { get; set; }

	public Transform target;

	private void Start ()
	{
		target.SetParent(null);
	}

	public void OnActivate ()
	{
		iTween.MoveTo(gameObject, new Hashtable{
			{iT.MoveTo.position, target},
			{iT.MoveTo.time, 3},
			{iT.MoveTo.easetype, iTween.EaseType.easeInOutCubic}
		});
	}

	public void OnDeactivate ()
	{
		iTween.Stop (gameObject);
	}
}
