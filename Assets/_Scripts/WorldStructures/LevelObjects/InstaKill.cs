﻿using UnityEngine;

public class InstaKill : MonoBehaviour
{
	public bool active = true;

	private void OnCollisionEnter2D (Collision2D hit)
	{
		if (!active)
			return;
		
		if(hit.gameObject.GetComponent<PlayerStats>() != null)
		{
			hit.gameObject.GetComponent<PlayerStats>().energy -= 10000;
		}
	}
}
