﻿using System.Collections;
using UnityEngine;

public class SinkablePlatform : MonoBehaviour
{
	private float speed = 0.5f;

	private float originalYPos;
	private float sinkedYPos;
	private new Rigidbody2D rigidbody;

	private bool isActivated;

	private void Awake()
	{
		rigidbody = GetComponent<Rigidbody2D>();
		originalYPos = transform.position.y;
		sinkedYPos = originalYPos - 1.2f;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			PlayerStats refStats = other.gameObject.GetComponent<PlayerStats>();
			if (refStats._color == PlayerColor.Red)
			{
				return;
			}
			else
			{
				StopAllCoroutines();
				isActivated = true;
				StartCoroutine(SinkRoutine());
			}
		}
	}
	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			PlayerStats refStats = other.gameObject.GetComponent<PlayerStats>();
			if (refStats._color == PlayerColor.Red)
			{
				return;
			}
			else
			{
				StopAllCoroutines();
				isActivated = false;
				StartCoroutine(FloatRoutine());
			}
		}
	}

	private IEnumerator FloatRoutine()
	{
		while (transform.position.y < originalYPos)
		{
			transform.Translate(Vector3.down * -1.5f * speed * Time.fixedDeltaTime);

			yield return new WaitForFixedUpdate();
		}
	}

	private IEnumerator SinkRoutine()
	{
		while (transform.position.y > sinkedYPos)
		{
			transform.Translate(Vector3.down * speed * Time.fixedDeltaTime);

			yield return new WaitForFixedUpdate();
		}
	}
}
