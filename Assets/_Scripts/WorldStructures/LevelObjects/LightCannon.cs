﻿using UnityEngine;
using System.Collections;
using System;

public class LightCannon : MonoBehaviour, IInteractable
{
	[SerializeField]
	private float damage = 20;
	[SerializeField]
	private float rotationTimer = 2f;
	[SerializeField]
	private float waitTimer = 1;
	[SerializeField]
	private float energyCost = 10;

	private ParticleSystem particle;
	private new Rigidbody2D rigidbody;
	private float damageRate = 0.5f;
	private float timer = 2;
	private AudioSource source;
	private bool isShooting;

	private void OnEnable()
	{
		rigidbody = GetComponent<Rigidbody2D>();
		source = GetComponentInChildren<AudioSource>();
		particle = GetComponentInChildren<ParticleSystem>();

		StartCoroutine(RotateRoutine(true));
	}

	private IEnumerator RotateRoutine(bool clockwise)
	{
		float targetPos = Mathf.RoundToInt((transform.eulerAngles.z + 180) % 360);
		float step = 180 / (rotationTimer * (1 / Time.fixedDeltaTime));
		if (!clockwise) step *= -1;
		float currentStep = transform.eulerAngles.z;

		for (float time = 0; time < rotationTimer; time += Time.fixedDeltaTime)
		{
			currentStep += step;
			rigidbody.MoveRotation((float)currentStep);

			yield return new WaitForFixedUpdate();
		}
		rigidbody.MoveRotation(targetPos);

		yield return new WaitForSeconds(waitTimer);
		StartCoroutine(RotateRoutine(!clockwise));
	}

	public void OnInteract(PlayerStats stats)
	{
		if (isShooting || !stats.isStrong) return;
		isShooting = true;
		stats.energy -= energyCost;

		source.Play();
		particle.Play();

		StartCoroutine(RayRoutine());
	}

	private IEnumerator RayRoutine()
	{
		for (float i = 0; i < timer; i += damageRate)
		{
			RaycastHit2D[] hit = Physics2D.RaycastAll(particle.transform.position, particle.transform.forward);
			for (int j = hit.Length - 1; j >= 0; j--)
			{
				if (hit[j].collider == null)
				{
					continue;
				}
				if (hit[j].collider.gameObject.GetComponent<IDamageable>() != null)
				{
					hit[j].collider.gameObject.GetComponent<IDamageable>().TakeDamage(damage);
				}
			}

			yield return new WaitForSeconds(damageRate);
		}

		isShooting = false;
	}
}
