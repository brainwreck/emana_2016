﻿using UnityEngine;
using System.Collections.Generic;

public class AmandiTrigger: MonoBehaviour
{
	[SerializeField][Multiline]
	private List<string> dialog;
	[SerializeField]
	private PlayerColor characterColor;

	private static GameObject amandiParticle;
	private Vector3 amandiPosition;
	private bool active;

	void Start () 
	{
		if (amandiParticle == null)
		{
			amandiParticle = Instantiate(PrefabsHolder.instance.amandiPrefab);
			amandiParticle.SetActive(false);
		}

		amandiPosition = transform.FindChild("AmandiPosition").position;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		PlayerStats stats = other.GetComponent<PlayerStats>();
        if (stats != null && stats._color == characterColor && !active)
		{
			active = true;
			amandiParticle.transform.position = amandiPosition;
			amandiParticle.SetActive(true);
			TextBox.instance.Activate(dialog, amandiPosition);
		}
	}

	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.GetComponent<PlayerStats>() != null && active)
		{
			active = false;
		}
	}
}
