﻿using UnityEngine;

public class DestructibleWall : MonoBehaviour, IActivated
{
	[SerializeField]
	private GameObject[] affectedObjects;

	private PlayerStats stats;
	private float speed;
    private bool playerActivated;

	private void OnTriggerEnter2D(Collider2D other)
	{
		stats = other.gameObject.GetComponent<PlayerStats>();
        if (stats != null)
		{
			if (stats.isStrong)
			{
				speed = stats.GetComponent<Rigidbody2D>().velocity.x;
				Crush();
			}
		}
	}

	private void Crush()
	{
		GetComponent<AudioSource>().Play();
		GetComponentInChildren<ParticleSystem>().Play();

		speed = Mathf.Abs(speed) < 1 ? 3f : speed;

		Collider2D[] colliders = GetComponents<Collider2D>();
		for (int i = 0; i < colliders.Length; i++)
		{
			colliders[i].enabled = false;
		}

		for (int j = 0; j < affectedObjects.Length; j++)
		{
			affectedObjects[j].GetComponent<Rigidbody>().useGravity = true;
			affectedObjects[j].GetComponent<Rigidbody>().isKinematic = false;
			affectedObjects[j].GetComponent<Rigidbody>().AddExplosionForce(speed * 100, stats != null ? stats.transform.position : transform.position, 5f);
		}

		Invoke("DisableObjects", 3);
	}

	private void DisableObjects()
	{
		for (int k = 0; k < affectedObjects.Length; k++)
		{
			affectedObjects[k].SetActive(false);
		}
	}

	public void OnActivate()
    {
        Crush();
    }

    public void OnDeactivate() { }
}
