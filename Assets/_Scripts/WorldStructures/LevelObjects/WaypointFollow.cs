﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;

[RequireComponent(typeof(Rigidbody2D))]
public class WaypointFollow : MonoBehaviour, IChange
{
	new private Rigidbody2D rigidbody;
	public Transform waypointsHolder;
	private Transform[] waypoints;
	public FollowMode _followMode = FollowMode.Loop;
	[Range(0, 666)]public float _timer = 0;
	public float _speed = 1;

	private void Awake ()
	{
		rigidbody = GetComponent<Rigidbody2D>();
		if(waypointsHolder != null)
		{
			List<Transform> transformsList = new List<Transform> ();
			foreach (var item in waypointsHolder.GetComponentsInChildren<Transform>())
			{
				if(item != waypointsHolder)transformsList.Add (item);
			}
			waypoints = transformsList.ToArray ();
			waypointsHolder.SetParent (null);
		}
	}
	private void Start ()
	{
		Follow();
	}


	public void Follow () { StartCoroutine (FollowWaypointsRoutine (_followMode)); }
	public void Follow (FollowMode mode) { StartCoroutine (FollowWaypointsRoutine (mode)); }
	private IEnumerator FollowWaypointsRoutine (FollowMode mode)
	{
		if (waypointsHolder == null || waypoints.Length == 0)
			yield break;

		Vector2 startPos, targetPos, deltaPos;

		for (int i = 0; i < waypoints.Length; i++)
		{
			startPos = transform.position;
			targetPos = waypoints [i].position;
			deltaPos = targetPos - startPos;

			if(_timer > 0)
			{
				for(float timer = 0; timer < _timer; timer += Time.fixedDeltaTime)
				{
					rigidbody.MovePosition (startPos + deltaPos * timer / _timer);
					yield return new WaitForFixedUpdate ();
				}
				transform.position = targetPos;
			}
			else if(_speed > 0)
			{
				Vector2 speed = deltaPos.normalized * _speed * Time.fixedDeltaTime;;
				while (((Vector2)transform.position - targetPos).sqrMagnitude > speed.sqrMagnitude)
				{
					speed = deltaPos.normalized * _speed * Time.fixedDeltaTime;
					rigidbody.MovePosition ((Vector2)transform.position + speed);
					yield return new WaitForFixedUpdate ();
				}
				transform.position = targetPos;
			}
		}

		if (mode == FollowMode.OneShot)
			yield break;

		if(mode == FollowMode.PingPong)
			Array.Reverse (waypoints);
		
		StartCoroutine(FollowWaypointsRoutine (mode));
	}
}

public enum FollowMode
{
	Loop,
	PingPong,
	OneShot
}
