﻿using UnityEngine;

public class Altar : MonoBehaviour, IInteractable
{
	public PlayerColor owner;
    public Door affectedDoor;
	[Multiline]
	public string correctCharFeedback;
	[Multiline]
	public string wrongCharFeedback;

	float activationDamage = 5000;

    private bool activated;

    public void OnInteract(PlayerStats stats)
    {
        if (stats._color == owner && !activated)
        {
            activated = true;

			AudioSource source = GetComponent<AudioSource>();
			source.volume = source.volume *= AudioOptions.sfxVolume;
			source.Play();
			GetComponentInChildren<Drain>().source = stats.gameObject.transform;
			GetComponentInChildren<Drain>().Blub();
            SetColors();
			TextBox.instance.Activate(correctCharFeedback, transform.position);

			Level level = GetComponentInParent<LevelController>().level;
			switch (stats._color)
			{
				case PlayerColor.Red:
					level.redFinished = true;
					break;
				case PlayerColor.Jade:
					level.jadeFinished = true;
					break;
				case PlayerColor.Blue:
					level.blueFinished = true;
					break;
			}

			LevelBuilder.instance.levelController.success = true;
            stats.energy -= activationDamage;
		}
		else
		{
			TextBox.instance.Activate(wrongCharFeedback, transform.position);
		}
    }

    private void SetColors()
    {
        // TODO: feedback
        if (affectedDoor != null) affectedDoor.GetComponent<IActivated>().OnActivate();
    }
}
