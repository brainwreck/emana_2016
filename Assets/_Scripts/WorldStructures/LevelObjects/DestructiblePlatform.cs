﻿using UnityEngine;
using System.Collections;

public class DestructiblePlatform : MonoBehaviour, IDestructible, IToggle, IChange
{
	public bool state { get; set; }
	public GameObject[] affectedObjects;
	public float timer = 1;

	private void Start ()
	{
		if(state)
		{
			foreach (var obj in affectedObjects)
			{
				obj.SetActive(false);
			}
			GetComponent<BoxCollider2D> ().enabled = false;
		}
	}

	public void Crush (PlayerColor color)
	{
		if(!state && color != PlayerColor.Red) StartCoroutine (CrushRoutine());
	}

	public IEnumerator CrushRoutine ()
	{
		yield return new WaitForSeconds (timer);
		state = true;
		GetComponent<AudioSource>().Play();
		GetComponentInChildren<ParticleSystem>().Play();
		foreach (var obj in affectedObjects)
		{
			obj.GetComponent<Rigidbody>().useGravity = true;
			obj.GetComponent<Rigidbody>().isKinematic = false;
			obj.GetComponent<Rigidbody>().AddExplosionForce (100, transform.position, 1.5f);
		}

		Invoke("DisableObjects", 5);
		GetComponent<BoxCollider2D> ().enabled = false;
	}

	private void DisableObjects()
	{
		for (int k = 0; k < affectedObjects.Length; k++)
		{
			affectedObjects[k].SetActive(false);
		}
	}
}
