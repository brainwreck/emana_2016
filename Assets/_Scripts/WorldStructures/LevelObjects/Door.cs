﻿using UnityEngine;

public class Door : MonoBehaviour, IActivated
{
	public void OnActivate()
	{
		GetComponent<DestructibleWall>().OnActivate();
	}

	public void OnDeactivate(){}
}
