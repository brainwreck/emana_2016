﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Pushable : MonoBehaviour, IPushable
{
	new private Rigidbody2D rigidbody;
	private AudioSource source;

	private void Awake ()
	{
		rigidbody = GetComponent<Rigidbody2D> ();
		source = GetComponent<AudioSource>();
	}

	public void Push (Vector2 force)
	{
		if (!source.isPlaying) source.Play();
		rigidbody.AddForce(force);
	}
}
