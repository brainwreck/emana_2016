﻿using UnityEngine;
using System.Collections;

public class RotatingPlatform : MonoBehaviour, IActivated
{
	[SerializeField]
	private float rotationTimer = 2f;
	[SerializeField]
	private float waitTimer = 1;
	[SerializeField]
	private bool buttonActivated = true;

	private new Rigidbody2D rigidbody;

    private void Awake ()
	{
		rigidbody = GetComponent<Rigidbody2D>();

		if (buttonActivated)
			gameObject.SetActive (false);
		else
			StartCoroutine(RotateRoutine());
	}

	public void OnActivate ()
	{
		if (buttonActivated)
		{
			gameObject.SetActive (true);
			StartCoroutine(RotateRoutine());
		}
	}
	public void OnDeactivate () {}

	private IEnumerator RotateRoutine ()
	{
		float targetPos = Mathf.RoundToInt((transform.eulerAngles.z + 90) % 360);
		float step = 90 / (rotationTimer * (1 / Time.fixedDeltaTime));
		float currentStep = transform.eulerAngles.z;

		for (float time = 0; time < rotationTimer; time += Time.fixedDeltaTime)
		{
			currentStep += step;
			rigidbody.MoveRotation((float)currentStep);

			yield return new WaitForFixedUpdate();
		}
		rigidbody.MoveRotation(targetPos);

		yield return new WaitForSeconds(waitTimer);
		StartCoroutine(RotateRoutine());
	}

}
