﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class MultiActivated : MonoBehaviour, IToggle
{
	public bool state { get; set; }

	public Toggleable[] toggles;
	public List<Toggleable> togglesList;
    public List<bool> togglesStates;

	public IActivated[] affectedObjects;

	private void Start ()
	{
        if (toggles.Length == 0) return;
		togglesList = new List<Toggleable> (toggles);
		togglesStates = new List<bool> ();
		for (int i = 0; i < togglesList.Count; i++)
		{
			togglesStates.Add(togglesList[i].state);
		}
	}

	public void ChangeState (Toggleable toggleObj)
	{
		togglesStates[togglesList.FindIndex (t => t.Equals (toggleObj))] = toggleObj.state;

		int max = togglesStates.Count;
		int addedBools = 0;
		for (int i = 0; i < togglesStates.Count; i++)
		{
			if (togglesStates [i])
				addedBools++;
		}

		if(addedBools == togglesStates.Count)
		{
			Activate ();
		}
	}

	protected virtual void Activate ()
	{
		state = true;

		foreach (var obj in affectedObjects)
		{
			obj.OnActivate();
		}
	}
}

