﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour, IChange, IActivated
{
	new private Rigidbody2D rigidbody;
	public Transform target;
	public float time = 2;
	public bool	triggerActivated;

	#region IActivated implementation
	public void OnActivate ()
	{
		StartCoroutine (PlatformRoutine (target.position));
	}
	public void OnDeactivate () {}
	#endregion

	private void Awake ()
	{
		rigidbody = GetComponent<Rigidbody2D> ();
	}
	private void Start ()
	{
		if(!triggerActivated && target != null) StartCoroutine (PlatformRoutine (target.position));
	}

	private IEnumerator PlatformRoutine (Vector3 moveTo)
	{
		Vector3 startPos = transform.position;
		Vector3 deltaPos = moveTo - startPos;
		float factor = 0;
		for(float timer = 0; timer < time; timer += Time.fixedDeltaTime)
		{	
			factor = timer / time;
			rigidbody.MovePosition (startPos + deltaPos * factor);
			yield return new WaitForFixedUpdate ();
		}
		rigidbody.MovePosition (moveTo);

		StartCoroutine (PlatformRoutine (startPos));
	}

	public IEnumerator MoveToRoutine (Vector3 moveTo)
	{
		Vector3 startPos = transform.position;
		Vector3 deltaPos = moveTo - startPos;
		float factor = 0;
		for(float timer = 0; timer < time; timer += Time.fixedDeltaTime)
		{
			factor = timer / time;
			rigidbody.MovePosition (startPos + deltaPos * factor);
			yield return new WaitForFixedUpdate ();
		}
		rigidbody.MovePosition (moveTo);

		yield return null;
	}
}
