﻿using System.Collections;
using UnityEngine;

public class Trapdoor : MonoBehaviour, IToggle, IDestructible
{
	public bool state { get; set; }
	public float timer = 2;

	private AudioSource source;

	private void Start()
	{
		source = GetComponent<AudioSource>();
		source.volume = source.volume *= AudioOptions.sfxVolume;
		if (state)
		{
			GetComponent<BoxCollider2D>().enabled = false;
		}
	}

	public void Crush(PlayerColor color)
	{
		if (!state) StartCoroutine(OpenTrapRoutine());
	}

	private IEnumerator OpenTrapRoutine()
	{
		Vector3 startPos = transform.position;

		yield return new WaitForSeconds(timer);

		transform.parent.GetComponentInChildren<ParticleSystem>().Play();
		source.Play();
		while ((transform.position.z - 2) < startPos.z)
		{
			transform.Translate(Vector3.back * Time.fixedDeltaTime);

			yield return new WaitForFixedUpdate();
		}

		GetComponent<Collider2D>().enabled = false;

		while ((transform.position.z - 4) < startPos.z)
		{
			transform.Translate(Vector3.back * Time.fixedDeltaTime);

			yield return new WaitForFixedUpdate();
		}
		transform.parent.GetComponentInChildren<ParticleSystem>().Stop();
		source.Stop();

		yield return null;
	}
}
