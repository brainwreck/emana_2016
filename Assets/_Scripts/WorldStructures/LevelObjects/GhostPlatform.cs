﻿using UnityEngine;
using System.Collections;

public class GhostPlatform : MonoBehaviour, IActivated
{
	[SerializeField]
	private Material[] materials = new Material[2];

	[SerializeField]
	private bool buttonActivated = false;

	[SerializeField]
	private float activeTime;
	[SerializeField]
	private float ghostTime;

	private bool activated;

	private void Start()
	{
		if (buttonActivated) ChangeState(false);
		else StartCoroutine(ActivateRepeating());
	}

	public void OnActivate()
	{
		activated = true;
		StartCoroutine(ActivateRepeating());
	}
	public void OnDeactivate(){}

	private IEnumerator ActivateRepeating()
	{
		float timer;
		
		for (;;)
		{
			timer = activated ? activeTime : ghostTime;
			ChangeState(activated);
			
			yield return new WaitForSeconds(timer);
			activated = !activated;
		}
	}

	private void ChangeState(bool state)
	{
		ParticleSystem particles = GetComponentInChildren<ParticleSystem>();
		AudioSource source = GetComponent<AudioSource>();

		if (state)
		{
			//source.Play();
			particles.Play();
		}

		GetComponent<Collider2D>().enabled = state;
		GetComponentInChildren<MeshRenderer>().material = state ? materials[1] : materials[0];
	}
}
