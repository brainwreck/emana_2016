﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MovingPlatform))]
public class Crusher : MonoBehaviour
{
	private MovingPlatform movingPlatform;
	private	InstaKill crusherHit;
	private ParticleSystem particles;
	private AudioSource hitAudio;
	private AudioSource dragAudio;
	public	Transform slowTarget, fastTarget;
	public	float	initialWait;
	public	float	slowTime = 3;
	public	float	fastTime = .3f;
	public	float	hitWait;

	private void Awake ()
	{
		particles = transform.parent.GetComponentInChildren<ParticleSystem>();
		hitAudio = GetComponentInChildren<AudioSource>();
		dragAudio = GetComponentInParent<AudioSource>();
		movingPlatform = GetComponent<MovingPlatform> ();
		crusherHit = GetComponentInChildren<InstaKill> ();
	}
	private void Start ()
	{
		StartCoroutine (CrushRoutine ());
	}

	private IEnumerator CrushRoutine ()
	{
		yield return new WaitForSeconds (initialWait);

		while(true)
		{
			dragAudio.Play();
			crusherHit.active = false;
			movingPlatform.time = slowTime;
			movingPlatform.StartCoroutine (movingPlatform.MoveToRoutine (slowTarget.position));
			yield return new WaitForSeconds (slowTime);

			dragAudio.Stop();
			crusherHit.active = true;
			movingPlatform.time = fastTime;
			movingPlatform.StartCoroutine (movingPlatform.MoveToRoutine (fastTarget.position));
			yield return new WaitForSeconds (fastTime);

			particles.Play();
			hitAudio.Play();
			yield return new WaitForSeconds (hitWait);
		}
	}
}

