﻿using UnityEngine;
using System.Collections;

public class TimedDisable : MonoBehaviour
{
	public float timer = 1.5f;

	private void OnEnable ()
	{
		StartCoroutine (DisableRoutine());
	}
	private void OnDisable ()
	{
		StopAllCoroutines ();
	}

	private IEnumerator DisableRoutine ()
	{
		yield return new WaitForSeconds (timer);
		gameObject.SetActive (false);
	}
}
