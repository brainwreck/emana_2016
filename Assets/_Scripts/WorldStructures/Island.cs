﻿using System;

[Serializable]
public class Island
{
	public int id;
	public Temple[] temples = new Temple[4];
	public int[] templeProgress = new int[4];

	public Island ()
	{
		
	}

	public bool GetTempleProgress(int templeId)
	{
		return GetTemple(templeId).finished;
	}

	private Temple GetTemple(int id)
	{
		for (int i = 0; i < temples.Length; i++)
		{
			if (id == temples[i].templeId) return temples[i];
		}

		return null;
	}

	public Level GetAvailableLevel ()
	{
		//return temples[progress].GetLevelProgress();
		return null;
	}
}
