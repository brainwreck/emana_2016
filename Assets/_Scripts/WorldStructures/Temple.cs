﻿using System;
using UnityEngine;

[Serializable]
public class Temple
{
	[Range(0, 3)]
	public int templeId;
	public Level[] levels = new Level[3];
	public int[] levelProgress = new int[3];
	public bool finished
	{
		get 
		{
			return (levelProgress[0] == 1 && levelProgress[1] == 1 && levelProgress[2] == 1);
		}
	}

	public Temple ()
	{
		
	}

	public bool GetLevelProgress (int levelId)
	{
		return GetLevel(levelId).finished;
	}

	private Level GetLevel(int levelId)
	{
		for (int i = 0; i < levels.Length; i++)
		{
			if (levelId == levels[i].levelId) return levels[i];
		}

		return null;
	}

	public Level GetLastLevel ()
	{
		return levels[2];
	}
}