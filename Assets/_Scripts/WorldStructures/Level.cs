﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Level
{
	[Range(0, 11)]
	public int levelId;
	private List<Tile> _tiles;
	public List<Tile> Tiles
	{
		get { return _tiles; }
		set { _tiles = value; }
	}
	public int JasparLife = 100, NadjaLife = 100, ZafiroLife = 100;
	public bool redFinished, jadeFinished, blueFinished;
	public bool finished
	{
		get
		{
			return (redFinished && jadeFinished && blueFinished);
		}
	}
	public PlayerColor[] playerOrder = new PlayerColor[3];

	public Level (int templeId, int id)
	{
		/*
		string ambiente, personagem, setor;

		switch (templeId / 3)
		{
			case 1: ambiente = "Floresta"; break;
			case 2: ambiente = "Caverna"; break;
			case 3: ambiente = "Gelo"; break;
			case 4: ambiente = "Vulcao"; break;
			default: ambiente = "Praia"; break;
		}
		switch(templeId % 3)
		{
			case 1: personagem = "Nadja"; break;
			case 2: personagem = "Zafiro"; break;
			default: personagem = "Jaspar"; break;
		}
		setor = id.ToString ();

		prefabName = string.Format ("Nivel_{0}_{1}_{2}", ambiente, personagem, setor);

		lastLevel = id == 3;

		availablePlayers = lastLevel ? new List<bool> (3) {false, false, false} : new List<bool> (3) {true, true, true};
		availablePlayers [templeId % 3] = lastLevel;

		Tile[] t = new Tile[1];
		for (int i = 0; i < 1; i++)
		{
			t[i] = new Tile();
		}
		Tiles = new List<Tile> (t);
		*/
	}

	public int GetPlayer ()
	{
		/*int i = -1;
		while(i == -1)
		{
			i = UnityEngine.Random.Range (0, availablePlayers.Count);
			if (availablePlayers[i] == false)
				i = -1;
		}
		return i;*/
		return 0;
	}
}