﻿using UnityEngine;

public class TrailController : MonoBehaviour, IColorProgress
{
	Color	darkTrailColor = new Color (.05f, .05f, .05f, 1);

	public Color	redEmissionColor, jadeEmissionColor, blueEmissionColor;
	public bool isRed { get; set; }
	public bool isJade { get; set; }
	public bool isBlue { get; set; }

	public MeshRenderer[] redStones;
	public MeshRenderer[] jadeStones;
	public MeshRenderer[] blueStones;

	public void Paint (bool paintRed, bool paintJade, bool paintBlue)
	{
		if(paintRed) isRed = true;
		if(paintJade) isJade = true;
		if(paintBlue) isBlue = true;

		UpdateStones ();
	}

	private void UpdateStones ()
	{
		foreach (var stone in redStones)
		{
			stone.material.SetColor("_Color", isRed ? Color.white : darkTrailColor);
			stone.material.SetColor("_EmissionColor", isRed ? redEmissionColor : darkTrailColor);
			stone.material.globalIlluminationFlags = MaterialGlobalIlluminationFlags.RealtimeEmissive;
		}

		foreach (var stone in jadeStones)
		{
			stone.material.SetColor("_Color", isJade ? Color.white : darkTrailColor);
			stone.material.SetColor("_EmissionColor", isJade ? jadeEmissionColor : darkTrailColor);
			stone.material.globalIlluminationFlags = MaterialGlobalIlluminationFlags.RealtimeEmissive;
		}

		foreach (var stone in blueStones)
		{
			stone.material.SetColor("_Color", isBlue ? Color.white : darkTrailColor);
			stone.material.SetColor("_EmissionColor", isBlue ? blueEmissionColor : darkTrailColor);
			stone.material.globalIlluminationFlags = MaterialGlobalIlluminationFlags.RealtimeEmissive;
		}
	}
}

