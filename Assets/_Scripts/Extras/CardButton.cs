﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CardButton : MonoBehaviour, IPointerExitHandler
{
	public Image _img;
	public Sprite fullSizeArt;
	public string poemTitle;
	[TextArea]public string poem;

	public void Clicked ()
	{
		CardsViewController.instance.ClickedCardButton (this);
	}

	#region IPointerExitHandler implementation

	public void OnPointerExit (PointerEventData eventData)
	{
		if(!GetComponent<Animator>().GetCurrentAnimatorStateInfo (0).IsName ("Base Layer.Disabled")) GetComponent<Animator> ().SetTrigger ("Normal");
	}

	#endregion
}
