﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DialogBoxController : MonoBehaviour {

	public Text DialogText;
	public Image DialogBox;
	public AudioClip DialogSound;
	private string currentDialog;
	private Coroutine currentRoutine;

	public void SetDialogText(string currText)
	{
		gameObject.SetActive (true);
		DialogText.text = " ";
		currentDialog = currText;
		if(currentRoutine != null)
			StopCoroutine (currentRoutine);
		currentRoutine = StartCoroutine (StartText ());
	}

	IEnumerator StartText()
	{
		for (int i = 0; i < currentDialog.Length; i++) {
			DialogText.text = DialogText.text + currentDialog [i];
			DialogText.text = DialogText.text.ToUpper ();
			RectTransform rt = DialogBox.GetComponent<RectTransform>();
			rt.sizeDelta = new Vector2(rt.rect.width, DialogText.preferredHeight);
			yield return new WaitForSeconds (0.025f);
		}
	}
}
