﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class PointerEnterExitHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public Image _img;
	public Sprite highlightedSprite, oldSprite;

	public void Awake ()
	{
		oldSprite = _img.sprite;
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		if(GetComponent<Button>().interactable)
			_img.sprite = highlightedSprite;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		_img.sprite = oldSprite;
	}
}
