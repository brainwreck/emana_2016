﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class Drain : MonoBehaviour {
    public Transform source;

    private ParticleSystem _system;

    void Start()
    {
        if (_system == null)
            _system = GetComponent<ParticleSystem>();
    }

    public void Blub()
    {
        StartCoroutine(GenerateParticles());
    }

    private IEnumerator GenerateParticles()
    {
        Vector3 dir = transform.position - source.position;

        _system.Emit(source.position, dir, 1, 1, Color.clear);
        
        yield return null;
    }
}
