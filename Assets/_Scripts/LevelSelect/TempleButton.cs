﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class TempleButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public Image _img;
	public Sprite normalSprite, highlightedSprite;

    public void OnPointerEnter(PointerEventData eventData)
	{
		if(GetComponent<Button>().interactable)
			_img.sprite = highlightedSprite;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		_img.sprite = normalSprite;
	}

}
