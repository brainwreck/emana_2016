﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(Image))]
public class MenuButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public Sprite highlightedSprite;
    public string label;
	private Sprite oldSprite;
	private Image _img;

	private void Awake ()
	{
		_img = GetComponent<Image> ();
		oldSprite = _img.sprite;
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
		_img.sprite = highlightedSprite;
		if(Application.loadedLevelName == SceneNames.MENU) MainMenuViewController.instance.SetLabel(label);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
		_img.sprite = oldSprite;
		if(Application.loadedLevelName == SceneNames.MENU) MainMenuViewController.instance.ResetLabel();
    }
}
