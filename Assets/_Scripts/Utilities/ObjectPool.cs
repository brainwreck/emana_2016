﻿using UnityEngine;
using System.Collections.Generic;

public class ObjectPool : MonoBehaviour
{
    public  GameObject  pooledObject;
    public  int         pooledAmount = 1;
    public  bool        willGrow = true;

    public  List<GameObject> pooledObjects;

    public void Init ()
    {
        pooledObjects = new List<GameObject> ();
        for (int i = 0; i < pooledAmount; i++)
        {
            GameObject obj = Instantiate(pooledObject) as GameObject;
            pooledObjects.Add(obj);
            obj.SetActive(false);
        }
    }

    public GameObject GetPooledObject ()
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy)
            {
                return pooledObjects[i];
            }
        }

        if (willGrow)
        {
            GameObject obj = Instantiate(pooledObject) as GameObject;
            pooledObjects.Add (obj);
            return obj;
        }

        return null;
    }

	public void DisableAll ()
	{
		for (int i = 0; i < pooledObjects.Count; i++)
		{
			pooledObjects[i].SetActive(false);
		}
	}
    
}
