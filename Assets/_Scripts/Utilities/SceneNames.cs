﻿public static class SceneNames
{
	public const string SPLASH = "SplashScreen";
	public const string INTRO = "Intro";
	public const string MENU = "MainMenu";
    public const string LEVELS = "LevelSelect";
	public const string GAME = "GameTest";
	public const string EXTRAS = "Extras";
	public const string CREDITS = "Credits";
	public const string SETTINGS = "Settings";
}
