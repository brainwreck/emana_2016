﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Reflection;

public static class ExtensionMethods
{
	public static IEnumerator Fade (this Image img, bool fadeToBlack, float fadeTime)
	{
		img.gameObject.SetActive (true);
		img.color = new Color (img.color.r, img.color.g, img.color.b, fadeToBlack ? 0 : 1);
		float targetAlpha = fadeToBlack ? 1 : 0;
		Color newColor;
		float factor = 0f;
		float newAlpha = 0f;

		if(fadeToBlack)
		{
			for(float timer = 0; timer < fadeTime; timer += Time.deltaTime)
			{
				factor = timer / fadeTime;
				newAlpha = targetAlpha * factor;
				newColor = new Color (img.color.r, img.color.g, img.color.b, newAlpha);
				img.color = newColor;
				yield return null;
			}
		}
		else
		{
			for(float timer = 0; timer < fadeTime; timer += Time.deltaTime)
			{
				factor = timer / fadeTime;
				newAlpha = 1 - factor;
				newColor = new Color (img.color.r, img.color.g, img.color.b, newAlpha);
				img.color = newColor;
				yield return null;
			}
		}
		
		img.gameObject.SetActive (fadeToBlack);
	}

	public static IEnumerator Fade (this Image img, float targetAlpha, float fadeTime)
	{
		float factor;
		float oldAlpha = img.color.a;
		float deltaAlpha = targetAlpha - oldAlpha;
		float newAlpha;
		Color newColor;
		for(float timer = 0; timer < fadeTime; timer += Time.deltaTime)
		{
			factor = timer / fadeTime;
			newAlpha = oldAlpha + deltaAlpha * factor;
			newColor = new Color (img.color.r, img.color.g, img.color.b, newAlpha);
			img.color = newColor;
			yield return null;
		}
		img.color = new Color (img.color.r, img.color.b, img.color.g, targetAlpha);
	}

	public static string RemoveFromEnd (this string s, string suffix)
	{
		if (s.EndsWith(suffix))
		{
			return s.Substring(0, s.Length - suffix.Length);
		}
		else
		{
			return s;
		}
	}
	
	public static string ClearPrefabSuffix (this string s)
	{
		s = s.Replace ("(Clone)", "");
		s = s.Trim();
		
		return s;
	}
	
	public static T GetCopyOf<T> (this Component comp, T other) where T : Component
	{
		Type type = comp.GetType();
		
		if (type != other.GetType()) return null; // type mis-match
		
		BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
		PropertyInfo[] pinfos = type.GetProperties(flags);
		foreach (var pinfo in pinfos) {
			if (pinfo.CanWrite) {
				try {
					pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
				}
				catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
			}
		}
		FieldInfo[] finfos = type.GetFields(flags);
		foreach (var finfo in finfos) {
			finfo.SetValue(comp, finfo.GetValue(other));
		}
		return comp as T;
	}

	// For copy/paste purposes
	const float EPSILON = 0.00001f;
}
