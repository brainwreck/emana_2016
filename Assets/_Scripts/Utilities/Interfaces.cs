﻿using UnityEngine;
using System;

public interface IColorProgress
{
	bool isRed { get; set; }
	bool isJade { get; set; }
	bool isBlue { get; set; }

	void Paint (bool paintRed, bool paintJade, bool paintBlue);
}

public interface IToggle
{
	bool state { get; set; }
}

public interface IPushable
{
	void Push (Vector2 force);
}

public interface ISpawnable
{
	float XPos { get; set; }
	float YPos { get; set; }
	float ZPos { get; set; }
	float XRot { get; set; }
	float YRot { get; set; }
	float ZRot { get; set; }
}

public interface IActivated
{
	void OnActivate ();
	void OnDeactivate ();
}

public interface IDestructible
{
	void Crush (PlayerColor color);
}

public interface IHittable
{
	void Hit ();
}

public interface IInteractable
{
	void OnInteract(PlayerStats stats);
}

public interface IChange { }

public interface IDamageable
{
	void TakeDamage(float damageValue);
}