﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TextBox : MonoBehaviour 
{
	private static TextBox _instance;
	public static TextBox instance 
	{
		get { return _instance; }
	}

	private List<string> texts = new List<string>();
	Text textBox;
	[SerializeField] Image textBg;
	[SerializeField] Image next;
	public Camera mainCam;
	private float displayDelay = 0.05f;
	private Vector3 offset = new Vector3(0, 140);

	private bool activated;
	public bool Activated { get { return activated; } }

	void Awake () 
	{
		_instance = this;
		textBox = GetComponentInChildren<Text>();
		texts = new List<string>();
		Deactivate();
	}

	public void Activate(string text, Vector3 speakerPos)
	{
		if (activated) return;

		activated = true;
		GameEventsManager.ShowDialog(true);
		mainCam = Camera.main;
		texts.Add(text);
		transform.position = mainCam.WorldToScreenPoint(speakerPos) + offset;
		textBox.enabled = true;
		textBg.enabled = true;

		StartCoroutine(DisplayText());
	}

	public void Activate(List<string> text, Vector3 speakerPos)
	{
		if (activated) return;

		activated = true;
		GameEventsManager.ShowDialog(true);
		mainCam = Camera.main;
		texts.AddRange(text);
		transform.position = mainCam.WorldToScreenPoint(speakerPos) + offset;
		textBox.enabled = true;
		textBg.enabled = true;

		StartCoroutine(DisplayText());
	}

	public void Deactivate()
	{
		activated = false;
		texts.Clear();
		textBox.text = string.Empty;
		textBox.enabled = false;
		textBg.enabled = false;
		next.enabled = false;
	}

	private IEnumerator DisplayText()
	{
		for (int i = 0; i < texts.Count; i++)
		{
			next.enabled = false;
			textBox.text = string.Empty;
			yield return new WaitForSeconds(displayDelay * 5);

			for (int j = 0; j < texts[i].Length; j++)
			{
				if (Input.anyKey)
				{
					textBox.text = texts[i];
					break;
				}
				else
				{
					textBox.text += texts[i][j];
				}

				yield return new WaitForSeconds(displayDelay);
			}

			yield return new WaitForSeconds(displayDelay * 5f);

			while (!Input.anyKeyDown)
			{
				next.enabled = true;
				yield return null;
			}
		}

		Deactivate();
		GameEventsManager.ShowDialog(false);
	}
}
