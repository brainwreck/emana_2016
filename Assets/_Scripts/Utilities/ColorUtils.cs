﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;

public class ColorUtils : MonoBehaviour
{
	public static ColorUtils instance;

	public Color redColor;
	public Color jadeColor;
	public Color blueColor;

	private void Awake ()
	{
		instance = this;
	}
}
