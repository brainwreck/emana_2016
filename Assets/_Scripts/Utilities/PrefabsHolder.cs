﻿using UnityEngine;
using System.Collections.Generic;

public class PrefabsHolder : MonoBehaviour
{
	public static PrefabsHolder instance;

	[Header ("Player Characters")]
	public	GameObject	JasparPrefab;
	public	GameObject	NadjaPrefab;
	public	GameObject	ZafiroPrefab;

	[Header ("Level Prefabs")]
	public	GameObject[]	trailPrefabs;
	public	GameObject[]	levelPrefabs;
	public	GameObject		gameCamera;

	[Header ("Spawnables")]
	public	GameObject	JasparOrb;
	public	GameObject	NadjaClone;
	public	GameObject	ZafiroTrail;

	[Header ("GameObjects")]
	[SerializeField]private	GameObject	bullet;
	public	ObjectPool	bulletPool;
	[SerializeField]private	GameObject	bulletExplostion;
	public	ObjectPool	bulletExplosionPool;
	public	GameObject	seekerPrefab;
	public GameObject	amandiPrefab;

	[Header("Default Text Feedbacks")][Multiline]
	public string deathText;

	private void Awake ()
	{
		instance = this;

		bulletPool = gameObject.AddComponent<ObjectPool>();
		bulletPool.pooledObject = bullet;
		bulletPool.Init ();

		bulletExplosionPool = gameObject.AddComponent<ObjectPool>();
		bulletExplosionPool.pooledObject = bulletExplostion;
		bulletExplosionPool.Init ();
	}

	public GameObject GetLevel (int index)
	{
		return levelPrefabs[index];
	}

	public GameObject GetRandomTrail ()
	{
		return trailPrefabs[Random.Range (0, trailPrefabs.Length)];
	}

	public GameObject GetPlayer (int index)
	{
		switch (index) {
		case 1:
			return NadjaPrefab;
		case 2:
			return ZafiroPrefab;
		default:
			return JasparPrefab;
		}
	}
}
