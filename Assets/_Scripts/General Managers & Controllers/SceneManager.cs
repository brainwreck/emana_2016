﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SceneManager
{
	protected static SceneManager instance;
	private MainManager mainManager;
	public Image fadeObj;

	protected SceneManager () {}
	public static SceneManager Instance
	{
		get
		{
			if (instance == null)
			{
				instance = new SceneManager ();
				instance.mainManager = GameObject.FindObjectOfType<MainManager> ();
				instance.fadeObj = instance.mainManager.fadeObj;
			}
			return instance;
		}
	}

	public void GoToSplash ()
	{
		FadeAndLoadScene (SceneNames.SPLASH);
	}
	public void GoToMenu ()
	{
		FadeAndLoadScene (SceneNames.MENU);
	}
    public void GoToLevelSelect ()
    {
        FadeAndLoadScene(SceneNames.LEVELS);
    }
    public void GoToGame (int i)
    {
        FadeAndLoadScene (GetLevelName(i));
    }
	public string GetLevelName (int id)
	{
        var a = id / 6f < 1f ? "Solacio" : "Silvas";
        var b = 1 + (Mathf.FloorToInt(id / 3f) % 2);
        var c = (id % 3 + 1).ToString();
        var d = D(id);
		Debug.Log (string.Format("{0}_{1}-{2}_{3}", a, b, c, d));
		return string.Format("{0}_{1}-{2}_{3}", a, b, c, d);
    }
	public void GoToGame ()
    {
		//var a = mainManager.TempleProgress / 6f < 1f ? "Solacio" : "Silvas";
		//var b = 1 + (Mathf.FloorToInt(mainManager.TempleProgress / 3f) % 2);
		//var c = (mainManager.TempleProgress % 3 + 1).ToString();
		//var d = D(mainManager.TempleProgress);
		//mainManager.StartCoroutine (FadeAndLoadScene (string.Format("{0}_{1}-{2}_{3}", a, b, c, d) ) );
		//mainManager.StartCoroutine (FadeAndLoadRoutine("GameTest"));
		//FadeAndLoadScene("GameTest");
	}
	private string D (int i)
    {
        switch (i % 3)
        {
            case 1: return "Zafiro";
            case 2: return "Nadja";
            default: return "Jaspar";
        }
    }

	public void GoToIntro ()
	{
		FadeAndLoadScene (SceneNames.INTRO);
	}

	public void LoadScene (string scene)
	{
		Time.timeScale = 1;
		Application.LoadLevel (scene);
	}

	public void FadeAndLoadScene (string scene)
	{
		mainManager.StartCoroutine (FadeAndLoadRoutine (scene));
	}
	private IEnumerator FadeAndLoadRoutine (string scene)
	{
		yield return mainManager.StartCoroutine (fadeObj.Fade (true, .4f));
		LoadScene (scene);
	}
}
