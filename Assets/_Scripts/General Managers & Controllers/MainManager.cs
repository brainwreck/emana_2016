﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class MainManager : MonoBehaviour
{
	public static MainManager instance;

	public Image fadeObj;

	//[HideInInspector]
	public int[] collectibles = new int[12];
	public int myProgress;
	public bool firstTime;

	private void Awake ()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy (this);
			return;
		}
		DontDestroyOnLoad (gameObject);

		EncryptedPlayerPrefs.keys=new string[5];
		EncryptedPlayerPrefs.keys[0]="udWrre23";
		EncryptedPlayerPrefs.keys[1]="up9DHaSP";
		EncryptedPlayerPrefs.keys[2]="rAA5S3fr";
		EncryptedPlayerPrefs.keys[3]="2eatprtH";
		EncryptedPlayerPrefs.keys[4]="eDw3Asja";

		AudioOptions.bgmVolume = PlayerPrefs.GetFloat("Music", .8f);
		AudioOptions.sfxVolume = PlayerPrefs.GetFloat("Sfx", 1f);

	}

	private void Start ()
	{
		if (Application.loadedLevelName != "Base")
			return;

		firstTime = EncryptedPlayerPrefs.GetInt ("firstTime", 0) == 0;

		LoadProgress ();
		SceneManager.Instance.GoToSplash();
	}

	public void SaveCollectibles()
	{
		for (int i = 0; i < collectibles.Length; i++)
		{
			EncryptedPlayerPrefs.SetInt(string.Format("collectible_{0}", i), collectibles[i]);
		}
	}

	public void LoadCollectibles()
	{
		for (int i = 0; i < collectibles.Length; i++)
		{
			collectibles[i] = EncryptedPlayerPrefs.GetInt(string.Format("collectible_{0}", i), 0);
		}
	}

	public void SaveProgress ()
	{
		EncryptedPlayerPrefs.SetInt("myProgress", myProgress);
	}

	public void LoadProgress ()
	{
		myProgress = EncryptedPlayerPrefs.GetInt("myProgress", 0);
	}

	public void OnLevelWasLoaded (int i)
	{
		StartCoroutine(fadeObj.Fade (false, .4f)); 
	}

	public void OnApplicationFocus (bool state)
	{
		if (!state)
			SaveProgress ();
	}
}
