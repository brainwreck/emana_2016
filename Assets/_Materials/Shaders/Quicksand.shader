﻿Shader "Custom/Quicksand" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_ScrollYSpeed ("Y Scroll Speed", Float) = 2
		_ScrollXSpeed ("X Scroll Speed", Float) = 0
		_Color("Color", Color) = (0, 0, 0, 0)
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0
		
		sampler2D _MainTex;
		float4 _Color;
		float _ScrollYSpeed;
		float _ScrollXSpeed;

		struct Input {
			float2 uv_MainTex;
		};		
		
		half _Glossiness;
		half _Metallic;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed2 scrolledUV = IN.uv_MainTex;
			
			float yScrollValue = _ScrollYSpeed * _Time;
			float xScrollValue = _ScrollXSpeed * _Time;
			
			scrolledUV += fixed2(xScrollValue, yScrollValue);
			
			half4 c = tex2D (_MainTex, scrolledUV);
			o.Albedo = c.rgb + _Color;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;		
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
